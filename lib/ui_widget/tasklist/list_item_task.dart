import 'package:flutter/material.dart';
import 'package:trux24/model/task.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class ItemTask extends StatelessWidget {
  final Task task;
  final VoidCallback onPressed;

  ItemTask({Key key, @required this.task, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
          onTap: onPressed,
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
              child: Row(children: <Widget>[
                //  FittedBox(
                //     fit: BoxFit.fitHeight,
                //     child: Container(width: 3,
                //        color: ColorsHelper.BLACK,
                //        ),

                //  ),

                Container(
                  constraints: BoxConstraints.expand(height: 135, width: 5),
                  color: ColorsHelper.AIR_BLUE_COLOR,
                ),

                Gaps.hGap5,
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 7),
                        child: Text("Apple and Android Application",
                            maxLines: 2,
                            style: TextStyle(
                                color: ColorsHelper.TRUX_GREY_5_COLOR,
                                fontSize: 16)),
                      ),

                      Gaps.vGap4,
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.add, size: 14),
                            ),
                            TextSpan(
                                text: " No of Trucks",
                                style: TextStyle(
                                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                                    fontSize: 13)),
                          ],
                        ),
                      ),
                      Gaps.vGap4,
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.add, size: 14),
                            ),
                            TextSpan(
                                text: " Open Truck, 3 Ton, 10 Feet",
                                style: TextStyle(
                                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                                    fontSize: 13)),
                          ],
                        ),
                      ),
                      //  Gaps.vGap4,
                      _buildRowAvaterView,
                      //  _textStatus,
                      Gaps.vGap6,

                      // Text(book.title,
                      //     style: TextStyle(color: grey3Color, fontSize: 16)),
                      // Gaps.vGap8,
                      // Text(book.shortIntro,
                      //     maxLines: 2,
                      //     overflow: TextOverflow.ellipsis,
                      //     style: TextStyle(color: grey6Color, fontSize: 14)),
                      // Gaps.vGap8,
                      // Row(children: <Widget>[
                      //   Expanded(
                      //     child: Row(mainAxisSize: MainAxisSize.min, children: <
                      //         Widget>[
                      //       Icon(Icons.person, size: 13, color: Colors.grey),
                      //       Gaps.hGap5,
                      //       Text('${book?.author}',
                      //           style:
                      //               TextStyle(color: grey9Color, fontSize: 14))
                      //     ]),
                      //   ),
                      //   book.tags.length > 0
                      //       ? TagView(tag: book.tags.first)
                      //       : TagView(tag: '限免'),
                      //   book.tags.length > 1
                      //       ? Row(children: <Widget>[
                      //           Gaps.hGap5,
                      //           TagView(tag: book.tags[1])
                      //         ])
                      //       : SizedBox()
                      // ])
                    ]))
              ]))),
    );
  }

  Widget _textStatus = new Container(
    margin: const EdgeInsets.all(0.0),
    padding: const EdgeInsets.fromLTRB(6, 3, 6, 3),
    decoration: BoxDecoration(
        color: ColorsHelper.ROUND_SHAPE_TASK_STATUS_GREEN_COLOR,
        borderRadius: BorderRadius.all(Radius.circular(25)),
        border: Border.all(width: 1, style: BorderStyle.none)),
    child: Text("Active",
        style: TextStyle(
            color: ColorsHelper.TASK_STATE_GREEN_DARK_COLOR, fontSize: 10)),
  );

  // Social Button Widget
  Widget _buildRowAvaterView = new Container(
    alignment: Alignment.topLeft,
    child: Row(
      children: <Widget>[
        new Expanded(
            flex: 8,
            child: Padding(
                padding: const EdgeInsets.only(left: 0.0, right: 5.0),
                child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Gaps.vGap4,
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.add, size: 14),
                            ),
                            TextSpan(
                                text: " Monday,Jul 11,2018 Monday",
                                style: TextStyle(
                                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                                    fontSize: 13)),
                          ],
                        ),
                      ),
                      Gaps.vGap4,
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(Icons.add, size: 14),
                            ),
                            TextSpan(
                                text: " Monday,Jul 11,2018",
                                style: TextStyle(
                                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                                    fontSize: 13)),
                          ],
                        ),
                      ),
                       Gaps.vGap4,
                      Container(
                        margin: const EdgeInsets.all(0.0),
                        padding: const EdgeInsets.fromLTRB(6, 3, 6, 3),
                        decoration: BoxDecoration(
                            color: ColorsHelper
                                .ROUND_SHAPE_TASK_STATUS_GREEN_COLOR,
                            borderRadius: BorderRadius.all(Radius.circular(25)),
                            border:
                                Border.all(width: 1, style: BorderStyle.none)),
                        child: Text("Active",
                            style: TextStyle(
                                color: ColorsHelper.TASK_STATE_GREEN_DARK_COLOR,
                                fontSize: 10)),
                      ),
                    ]))),
        new Expanded(
          flex: 2,
          child: Container(
              alignment: Alignment.bottomRight,
              padding: const EdgeInsets.only(left: 5.0, right: 10.0),
              child: CircleButton(
                fillColor: ColorsHelper.TRUX_GREY_DARK_COLOR,
                iconColor: ColorsHelper.WHITE,
                onPressedAction: () {
                  debugPrint('avator');
                },
                 widget:  Icon(
                    FontAwesome.male,
                    color: ColorsHelper.WHITE,
                    size: 20,
                  ),
                elevation: 5.0,
                highlightElevation: 3,
                size: 45.0,
              )),
        ),
      ],
    ),
  );
}
