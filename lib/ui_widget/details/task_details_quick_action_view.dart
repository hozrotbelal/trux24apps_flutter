
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/widgets/circle_button.dart';


class DetailsQuickActionTask extends StatelessWidget {
  final VoidCallback onPressed;
  final Color textColor;
  final Color bgColor;
  final String boxText;
  final double boxTextFontSize;
  final String bottomText;
  final double bottomTextFontSize;
  final double circleButtonSize;
  final double boxBorderRadious;
  final Widget widget;
  final EdgeInsetsGeometry boxPadding;
  final EdgeInsetsGeometry boxMargin;

  DetailsQuickActionTask({
    Key key,
    this.onPressed,
    this.textColor,
    this.bgColor,
    this.boxText,
    this.boxTextFontSize,
    this.bottomText,
    this.bottomTextFontSize,
    this.circleButtonSize,
    this.boxBorderRadious = 0.0,
    this.boxPadding,
    this.boxMargin,
    this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                    margin: boxMargin ?? const EdgeInsets.fromLTRB(5, 0, 5, 0),
                    padding: boxPadding ?? const EdgeInsets.fromLTRB(5, 5, 5, 5),
        
                    child: CircleButton(
                    fillColor: bgColor ?? ColorsHelper.TRUX_GREY_DARK_COLOR,
                    iconColor: ColorsHelper.WHITE,
                    onPressedAction: () {
                      debugPrint('avator');
                    },
                    widget: widget ?? Text(
                                  boxText ?? "",
                                  style: TextStyle(fontSize:  boxTextFontSize ?? 14, color: textColor ?? ColorsHelper.WHITE),
                        ),
                    elevation: 5.0,
                    highlightElevation: 3,
                    size: circleButtonSize ?? 45.0,
                ),
                // Text(boxText ?? "",
                //         style: TextStyle(
                //             color: textColor ?? ColorsHelper.WHITE, fontSize: boxTextFontSize ?? 14))
                            ),
                Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: Text(bottomText?? "",
                            style: TextStyle(
                                color: textColor ?? ColorsHelper.WHITE,
                                fontSize: bottomTextFontSize ?? 14)))),
              ]),
        ));
  }
}
