import 'package:flutter/material.dart';
import 'package:trux24/enum/enum.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:trux24/widgets/line.dart';

class DetailsPostedByTask extends StatelessWidget {

  final VoidCallback onPressed;
  final Color lineColor;
  final double circleButtonSize;
  final Widget widget;

  DetailsPostedByTask({Key key, 
  this.onPressed,
  this.lineColor, 
  this.circleButtonSize, 
  this.widget,

  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(5.0),
            width: double.infinity,
            child: Row(
               crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Align (
                  alignment: Alignment.topLeft,
                  child: CircleButton(
                    fillColor: ColorsHelper.TRUX_GREY_DARK_COLOR,
                    iconColor: ColorsHelper.WHITE,
                    onPressedAction: () {
                      debugPrint('avator');
                    },
                    widget: widget ?? Text(
                                  'TX',
                                  style: TextStyle(fontSize: 15, color: ColorsHelper.WHITE),
                        ),
                    elevation: 5.0,
                    highlightElevation: 3,
                    size: circleButtonSize ?? 45.0,
                )),
                Gaps.hGap10,
                Expanded(
                 child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[   
                   Row(
                      children: <Widget>[
                        Expanded(
                          child:  Text(
                          UIStringHelper.postTaskDetailsPostByLabel,
                          style: TextStyle(
                            color: ColorsHelper.TRUX_GREY_4_COLOR,
                            fontSize: 13,
                          )),
                        ),
                        Text(
                       UIStringHelper.postTaskDetailsTripIdLabel,
                          style: TextStyle(
                            color: ColorsHelper.TRUX_GREY_4_COLOR,
                            fontSize: 13,
                          ),
                        ),
                                            Gaps.hGap5,

                      ],
                    ),
                    Gaps.vGap5,
                    Row(
                      children: <Widget>[
                        Expanded(
                         child:  Text(
                          'Hozrot Belal',
                          style: TextStyle(
                            color: ColorsHelper.PRIMARY_COLOR,
                            fontSize: 15,
                          )),
                        ),
                        Text(
                           UIStringHelper.postTaskDetailsNowLabel,
                          style: TextStyle(
                            color: ColorsHelper.TRUX_GREY_4_COLOR,
                            fontSize: 13,
                          ),
                        ),
                        Gaps.hGap5,
                      ],
                    ),
                     Container(
                  margin: const EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),

              child:    LineWidget(
                    lineType: LineType.horizontal,
                    color: lineColor ?? ColorsHelper.TRUX_GREY_3_COLOR,
                    height: 1.0,
                    width:  double.infinity)),
                  ],
                )),
              ],
            ),
          ),
        ));
  }
}
