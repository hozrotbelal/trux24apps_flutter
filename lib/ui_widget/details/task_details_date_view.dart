import 'package:flutter/material.dart';
import 'package:trux24/enum/enum.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/widgets/line.dart';

class TaskDetailsDateView extends StatelessWidget {

  final VoidCallback onPressed;
   final Widget iconWidget;
  final Widget widget;
  final String iconUrl;
  final double iconSize;
  final Color iconColor;
  final Color lineColor;
  TaskDetailsDateView({Key key,  
   this.onPressed, 
   this.iconWidget,
   this.iconUrl,
   this.iconSize, 
   this.iconColor,
   this.lineColor,
   this.widget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(5.0),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                      //padding: EdgeInsets.all(3),
                      height: 45,
                      child: iconWidget ?? ImageIcon(
                              AssetImage("assets/images/plus_icon.png"),
                              size: iconSize ?? 15,
                              color: iconColor ?? ColorsHelper.BLACK,
                          ),
                ),
                Gaps.hGap10,
                Expanded(
                 child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[   
                    widget ?? Text(
                          '',
                          style: TextStyle(
                          color: ColorsHelper.TRUX_GREY_4_COLOR,
                          fontSize: 13,
                          ),
                        ),
            Container(
                      margin: const EdgeInsets.only(left: 7, right: 7, top: 8, bottom: 2),

                  child:    LineWidget(
                        lineType: LineType.horizontal,
                        color: lineColor ?? ColorsHelper.TRUX_GREY_3_COLOR,
                        height: 1.0,
                        width:  double.infinity)),
                   
                  ],
                 
                ),
                
                ),
              ],
            ),
          ),
        ));
  }
}
