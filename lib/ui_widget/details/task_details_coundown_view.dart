
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/widgets/circle_button.dart';


class DetailsCountDownTask extends StatelessWidget {
  final VoidCallback onPressed;
  final Color textColor;
  final Color bgColor;
  final String boxText;
  final double boxTextFontSize;
  final String bottomText;
  final double bottomTextFontSize;
  final double circleButtonSize;
  final double boxSizeHeight;
  final double boxSizeWidth;
  final double boxBorderRadious;
  final Widget widget;
  final EdgeInsetsGeometry boxPadding;
  final EdgeInsetsGeometry boxMargin;

  DetailsCountDownTask({
    Key key,
    this.onPressed,
    this.textColor,
    this.bgColor,
    this.boxText,
    this.boxTextFontSize,
    this.bottomText,
    this.bottomTextFontSize,
    this.circleButtonSize,
    this.boxSizeHeight = 0.0,
    this.boxSizeWidth = 0.0,
    this.boxBorderRadious = 0.0,
    this.boxPadding,
    this.boxMargin,
    this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    margin: boxMargin ?? const EdgeInsets.fromLTRB(5, 0, 5, 0),
                    padding: boxPadding ?? const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    width: boxSizeWidth ?? 0.0,
                    height: boxSizeHeight ?? 0.0,
                    decoration: BoxDecoration(
                        color: bgColor ?? ColorsHelper.WHITE ,
                        borderRadius: BorderRadius.all(Radius.circular(boxBorderRadious ?? 0.0)),
                        border: Border.all(width: 1, style: BorderStyle.none)),
                    child: widget ??  Text(boxText ?? "",
                        style: TextStyle(
                            color: textColor ?? ColorsHelper.WHITE, fontSize: boxTextFontSize ?? 14))
                            ),
                Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                        child: Text(bottomText?? "",
                            style: TextStyle(
                                color: textColor ?? ColorsHelper.WHITE,
                                fontSize: bottomTextFontSize ?? 14)))),
              ]),
        ));
  }
}
