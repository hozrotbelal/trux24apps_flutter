import 'package:flutter/material.dart';
import 'package:trux24/enum/enum.dart';
import 'package:trux24/model/review.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:trux24/widgets/line.dart';
import 'package:trux24/widgets/ratingbar/smooth_star_rating.dart';

class DetailsOfferByTask extends StatelessWidget {
  final VoidCallback onPressed;
  final Color textColor;
  final Color bgColor;
  final String boxText;
  final double boxTextFontSize;
  final String bottomText;
  final double bottomTextFontSize;
  final double circleButtonSize;
  final double boxBorderRadious;
  final Widget widget;
  final EdgeInsetsGeometry boxPadding;
  final EdgeInsetsGeometry boxMargin;

  DetailsOfferByTask({
    Key key,
    this.onPressed,
    this.textColor,
    this.bgColor,
    this.boxText,
    this.boxTextFontSize,
    this.bottomText,
    this.bottomTextFontSize,
    this.circleButtonSize,
    this.boxBorderRadious = 0.0,
    this.boxPadding,
    this.boxMargin,
    this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <
              Widget>[
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: <
                Widget>[
              Container(
                  alignment: Alignment.center,
                  margin: boxMargin ?? const EdgeInsets.fromLTRB(2, 0, 3, 0),
                  padding: boxPadding ?? const EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child: CircleButton(
                    fillColor: bgColor ?? ColorsHelper.TRUX_GREY_DARK_COLOR,
                    iconColor: ColorsHelper.WHITE,
                    onPressedAction: () {
                      debugPrint('avator');
                    },
                    widget: widget ??
                        Text(
                          boxText ?? "TX",
                          style: TextStyle(
                              fontSize: boxTextFontSize ?? 14,
                              color: textColor ?? ColorsHelper.WHITE),
                        ),
                    elevation: 5.0,
                    highlightElevation: 3,
                    size: circleButtonSize ?? 45.0,
                  )),
              new Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Md. Hozrot Belal",
                                    style: TextStyle(
                                      color: ColorsHelper.PRIMARY_COLOR,
                                      fontSize: 13,
                                    )),
                                Gaps.vGap2,
                                Row(children: <Widget>[
                                  SmoothStarRating(
                                      rating: 3.5,
                                      size: 16,
                                      allowHalfRating: false,
                                      color: ColorsHelper.PRIMARY_COLOR),
                                  Gaps.hGap3,
                                  Text('(0)',
                                      style: TextStyle(
                                        color: ColorsHelper.TRUX_GREY_3_COLOR,
                                        fontSize: 13,
                                      )),
                                ]),
                                Gaps.vGap2,
                                Text('7% Completion Rate',
                                    style: TextStyle(
                                      color: ColorsHelper.TRUX_GREY_3_COLOR,
                                      fontSize: 13,
                                    )),
                                Gaps.vGap2,
                                Row(children: <Widget>[
                                  Text("7 hours ago",
                                      style: TextStyle(
                                          color: ColorsHelper.TRUX_GREY_3_COLOR,
                                          fontSize: 13,
                                          )),
                                              Gaps.vGap3,
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child: Container(
                                              width: 17,
                                              height: 17,
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 0, 5, 0),
                                              child: Icon(
                                                Icons.reply,
                                                size: 16,
                                                color: ColorsHelper
                                                    .TRUX_GREY_3_COLOR,
                                              )),
                                        ),
                                        TextSpan(
                                            text: "Reply",
                                            style: TextStyle(
                                                color: ColorsHelper
                                                    .TRUX_GREY_3_COLOR,
                                                fontSize: 13)),
                                      ],
                                    ),
                                  ),
                                ]),
                              ]),
                        ]),
                  )),
              new Expanded(
                flex: 2,
                child: Container(
                  alignment: Alignment.bottomRight,
                  padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(children: <Widget>[
                          Expanded(
                              child: Text("৳50000",
                                  style: TextStyle(
                                      color: ColorsHelper.PRIMARY_COLOR,
                                      fontSize: 16,
                                      decoration: TextDecoration.lineThrough))),
                          Gaps.hGap2,
                          Text(
                            "৳500000",
                            style: TextStyle(
                              color: ColorsHelper.TRUX_GREY_5_COLOR,
                              fontSize: 16,
                            ),
                          ),
                        ]),
                        Container(
                            alignment: Alignment.bottomRight,
                            margin:
                                const EdgeInsets.only(left: 7.0, right: 0.0),
                            child: ButtonWidget(
                              color: ColorsHelper.WHITE,
                              shadow: UIHelper.MUZ_BUTTONSHADOW,
                              borderSideColor: ColorsHelper.TRUX_GREY_3_COLOR,
                              textColor: ColorsHelper.PRIMARY_COLOR,
                              text:UIStringHelper.postTaskDetailsStatusActive,
                              topPadding: 0,
                              boxHeight: 40,
                              circleAvatar: false,
                              padding: EdgeInsets.only(
                                  left: 2, top: 0, bottom: 0, right: 2),
                              onPressed: () {
                                // Navigator.pop(context);
                                // Navigator.push(
                                //     context, MaterialPageRoute(builder: (context) => MainMenuScreen()));

                                print("Task Details");
                              },
                            )),
                      ]),
                ),
              ),
            ]),
            Align(
                alignment: Alignment.topCenter,
                child: Container(
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: Text(bottomText ?? "",
                        style: TextStyle(
                            color: textColor ?? ColorsHelper.WHITE,
                            fontSize: bottomTextFontSize ?? 14)))),
          ]),
        ));
  }
}
