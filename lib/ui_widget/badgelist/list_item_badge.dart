import 'package:flutter/material.dart';
import 'package:trux24/model/review.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/widgets/line.dart';

class BadgeItemTask extends StatelessWidget {
  final Review review;
  final VoidCallback onPressed;

  BadgeItemTask({Key key, @required this.review, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(5.0),
            width: double.infinity,
            child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          padding: new EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                          child: Icon(
                            Icons.call,
                            size: 25,
                            color: ColorsHelper.AIR_PINK_COLOR,
                          )),
                      Gaps.hGap10,
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            '00801714524535',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR,
                                fontSize: 16),
                          ),
                          Gaps.vGap5,
                          Text("153 days ago",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: ColorsHelper.TRUX_GREY_4_COLOR)),
                                   Gaps.vGap2,
                        ],
                      )),
                    ],
                  ),
                  Line(
                      margin: EdgeInsets.symmetric(vertical: 7),
                      color: Colors.grey),
                ]),
          ),
        ));
  }
}
