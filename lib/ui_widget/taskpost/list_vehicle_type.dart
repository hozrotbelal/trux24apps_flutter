import 'package:flutter/material.dart';
import 'package:trux24/model/taskpost/vehicle_type.dart';
import 'package:trux24/res/color_constant.dart';

class ItemVehicleSelector extends StatefulWidget {
  final List<VehicleType> mVehicleTypeList;
  final VoidCallback onPressed;

  //ItemVehicleSelector({Key key, @required this.vehicleType, this.onPressed}) : super(key: key);

  ItemVehicleSelector({Key key, this.mVehicleTypeList,this.onPressed})
      : assert(mVehicleTypeList != null),
        super(key: key);
  @override
  _VehicleSelectorState createState() => _VehicleSelectorState();
}

class _VehicleSelectorState extends State<ItemVehicleSelector> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: widget.mVehicleTypeList.length,
      itemBuilder: (ctx, i) {
        return GestureDetector(
          onTap: () {
            setState(() {
              _selectedIndex = i;
            });
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
           // padding:  EdgeInsets.fromLTRB(4, 0, 4, 0),
            decoration: BoxDecoration(
                color: _selectedIndex == i
                    ? ColorsHelper.PRIMARY_COLOR
                    : ColorsHelper.WHITE,
                borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                border: _selectedIndex == i
                    ? Border.all(color: ColorsHelper.PRIMARY_COLOR)
                    : Border.all(color: ColorsHelper.TRUX_GREY_4_COLOR)),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                splashColor: Colors.white24,
                borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                onTap: () {
                  setState(() {
                    _selectedIndex = i;
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 3, bottom: 3, left: 18, right: 18),
                  child: Center(
                    child: Text(
                      widget.mVehicleTypeList[i].name,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        //fontWeight: FontWeight.w600,
                        fontSize: 12,
                        letterSpacing: 0.27,
                        color: _selectedIndex == i
                            ? ColorsHelper.WHITE
                            : ColorsHelper.TRUX_GREY_4_COLOR,
                      ),
                    ),
                  ),
                ),
              ),
            ),

            // margin: const EdgeInsets.only(right: 9),
            // alignment: Alignment.center,
            // height: 35,
            // width: 35,
            // decoration: BoxDecoration(
            //   border: _selectedIndex == i ? Border.all(width: 2.5) : Border(),
            //   color: Colors.white,
            //   shape: BoxShape.circle,
            // ),
            // child: Text("${products[widget.id].size[i]}"),
          ),
        );
      },
    );
  }
}
