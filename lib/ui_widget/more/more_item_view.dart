import 'package:flutter/material.dart';
import 'package:trux24/model/option_list_item.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/screen/otp_screen.dart';
import 'package:trux24/ui_widget/bouncing_view.dart';
import 'package:trux24/utils/utils.dart';

class MoreItemView extends StatelessWidget {
  final OptionListItem optionListItem;
  final Color textColor;

  MoreItemView(this.optionListItem, {Key key, this.textColor = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
   
    return BouncingView(
        child: Container(
            color: ColorsHelper.WHITE,
            width: Utils.width,
            padding: EdgeInsets.all(0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ListTile(
                          title: Text(optionListItem.title,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: ColorsHelper.TRUX_GREY_5_COLOR),
                                  maxLines: 1),
                
                        //  onTap: () => Utils.launchURL('tel:$_phone'),
                          trailing: Row(children: <Widget>[
                         //   Text(_phone),
                         Gaps.hGap10,
                            Icon(Icons.keyboard_arrow_right,color: ColorsHelper.TRUX_GREY_4_COLOR)
                          ], mainAxisSize: MainAxisSize.min)),
                         Container(height: 0.5, color: Colors.grey[200]),

                ])),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => OtpScreen()));
        });
  }
}
