import 'package:flutter/material.dart';

class BouncingView extends StatefulWidget {
  final Widget child;
  final VoidCallback onPressed;
/// change factor
/// When the value is greater than 1, the sub-control changes: the default size first becomes larger and then release the button to return to the original size
/// When the value is less than 1, the sub-control changes: the default size is first reduced and then released to return to the original size
/// When the value is 1, the size of the child control is always unchanged   ///
/// 
  final double scaleFactor;

  BouncingView({
    Key key,
    @required this.child,
    @required this.onPressed,
    this.scaleFactor: 0.98,
  }) : super(key: key);

  @override
  createState() => _BouncingViewState();
}

class _BouncingViewState extends State<BouncingView>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;

  GlobalKey _childKey = GlobalKey();

  Widget get child => widget.child;

  VoidCallback get onPressed => widget.onPressed;

  double get scaleFactor => widget.scaleFactor;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));

    _animation = Tween<double>(begin: 1, end: scaleFactor).animate(_controller);
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPressed,
          onTapDown: (details) {
          _controller.forward(); // Play animation when clicked
        },
        onTapCancel: () {
          _controller.reverse(); // The rebound animation when canceling
        },
        child: ScaleTransition(
            key: _childKey,
            scale: _animation, // Define animation
            child: child));
  }
}
