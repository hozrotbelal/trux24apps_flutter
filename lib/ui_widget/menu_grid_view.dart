import 'package:flutter/material.dart';
import 'package:trux24/model/vehicle_category.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/screen/otp_screen.dart';
import 'package:trux24/screen/taskpost/taskpost.dart';
import 'package:trux24/screen/taskpost/taskpost2.dart';
import 'package:trux24/ui_widget/bouncing_view.dart';
import 'package:trux24/utils/utils.dart';
import 'package:trux24/widgets/imageloader/image_load_view.dart';
import 'package:trux24/enum/enum.dart';

class MenuGridView extends StatelessWidget {
  final VehicleCategory movie;
  final Color textColor;

  MenuGridView(this.movie, {Key key, this.textColor = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = (Utils.width - 6 * 2 - 5 * 2) / 3;
    double height = width * 383 / 270;
    print(movie);
    return BouncingView(
        child: Container(
            color: ColorsHelper.WHITE,
            width: width,
            padding: EdgeInsets.all(0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ImageLoadView(movie.iconUrl,
                      // imageType : ImageType.assets,
                      fit: BoxFit.contain,
                      padding: EdgeInsets.all(2),
                      margin: EdgeInsets.fromLTRB(2, 5, 2, 5),
                      width: 100,
                      height: 95),
                  Gaps.hGap10,
                  Container(
                      width: Utils.width,
                      color: ColorsHelper.TRUX_GREY_TRANSPARENT_COLOR,
                      padding: EdgeInsets.fromLTRB(0, 7, 0, 7),

                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
                              child: Text(movie.iconName,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: ColorsHelper.TRUX_GREY_4_COLOR),
                                  maxLines: 1),
                            ),
                            Gaps.hGap3,
                            Padding(
                                padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
                                child: Text(movie.iconName,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: ColorsHelper.TRUX_GREY_4_COLOR),
                                    maxLines: 1)),
                            Gaps.hGap5,
                          ]))
                ])),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => TaskPost2Screen()));
        });
  }
}
