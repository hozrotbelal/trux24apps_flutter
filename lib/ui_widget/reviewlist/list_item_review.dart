import 'package:flutter/material.dart';
import 'package:trux24/model/review.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:trux24/widgets/ratingbar/smooth_star_rating.dart';

class ReviewItemTask extends StatelessWidget {
  final Review review;
  final VoidCallback onPressed;

  ReviewItemTask({Key key, @required this.review, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.white,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(5.0),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CircleButton(
                  fillColor: ColorsHelper.TRUX_GREY_DARK_COLOR,
                  iconColor: ColorsHelper.WHITE,
                  onPressedAction: () {
                    debugPrint('avator');
                  },
                  widget:  Icon(
                    FontAwesome.male,
                    color: ColorsHelper.WHITE ?? Theme.of(context).accentColor,
                    size: 20,
                  ),
                
                  elevation: 5.0,
                  highlightElevation: 3,
                  size: 45.0,
                ),
                Gaps.hGap10,
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Heavy Covered van, 12 Ton, 18 Feet',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ColorsHelper.TRUX_GREY_DARK_COLOR,
                          fontSize: 16),
                    ),
                    Gaps.vGap5,
                    Text("Good Job",
                        style: TextStyle(
                            fontSize: 14,
                            color: ColorsHelper.TRUX_GREY_4_COLOR)),
                    Gaps.vGap5,
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: SmoothStarRating(
                              rating: 3.5,
                              size: 18,
                              allowHalfRating: false,
                              color: ColorsHelper.PRIMARY_COLOR),
                          // child: Text("Good Job",
                          //     style: TextStyle(
                          //         fontSize: 14,
                          //         color: ColorsHelper
                          //             .TRUX_GREY_4_COLOR))
                        ),
                        Text(
                          '153 days ago',
                          style: TextStyle(
                            color: ColorsHelper.TRUX_GREY_4_COLOR,
                            fontSize: 13,
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
              ],
            ),
          ),
        ));
  }
}
