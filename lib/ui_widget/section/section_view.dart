import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';

class SectionView extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final String more;
  final bool hiddenMore;
  final Color textLeftHeaderColor;
  final Color textColor;
  final EdgeInsetsGeometry padding;
  final Widget child;
  final BoxDecoration decoration;
  final BoxDecoration headerDecoration;
  final double borderWidth;
  final double borderRadius;
 // final int borderRadiusValue ;
  final Color headerBgTopColor;
  final Widget leftWidgetHeader;

  SectionView(
    this.title, {
    Key key,
    this.onPressed,
    this.more: "More",
    this.hiddenMore: false,
    this.textLeftHeaderColor = Colors.black,
    this.textColor = Colors.black,
    this.padding,
    this.decoration,
    this.headerDecoration ,
    this.borderWidth,
    this.borderRadius ,
   // this.borderRadiusValue,
    this.headerBgTopColor,
    this.leftWidgetHeader,
    @required this.child,
  })  : assert(title != null, child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var borderRadius2 = borderRadius;

                return Container(
                    decoration: decoration ?? myBoxDecoration(),
                    child: Column(
                      children: <Widget>[
                       
                         ClipRRect(
                                  borderRadius:  new BorderRadius.only(
                                   topLeft:  Radius.circular(borderRadius ?? 0.0) ,
                                   topRight:  Radius.circular(borderRadius ?? 0.0),
                    ),
                                        
            child: Container(
                color: headerBgTopColor ?? ColorsHelper.WHITE,
              
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(
                            left: 1.5, right: 1.5, top:0, bottom: 0),
                          padding: padding ?? EdgeInsets.fromLTRB(5, 5, 5, 5),

                        child: Row(children: <Widget>[
                          leftWidgetHeader ?? Text(""),
                          Text(
                            "$title",
                            style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: textLeftHeaderColor,
                               ),
                          ),
                        ]),
                      ),
                      Offstage(
                          child: InkWell(
                              child: Container(
                                height: 36,
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text('$more',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: textColor)),
                                      Gaps.hGap3,
                                      Icon(CupertinoIcons.forward,
                                          size: 18, color: textColor)
                                    ]),
                              ),
                              onTap: onPressed),
                          offstage: hiddenMore),
                    ]))),
            child
          ],
        ));
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        width: borderWidth ?? 0.0,
        color: headerBgTopColor ?? ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius ?? 0.0),
        //                 <--- border radius here
      ),
    );
  }

   BoxDecoration headerBoxDecoration() {
    return BoxDecoration(
        border: Border.all(
        width: 0.0,
        color: headerBgTopColor ??ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(15 ?? 0.0),
       
      ),
    );
  }
}
