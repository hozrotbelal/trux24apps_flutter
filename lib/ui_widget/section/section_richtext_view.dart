import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';


class SectionRichTextView extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final String more;
  final double leftTextSize;
  final bool hiddenMore;
  final Color textColor;
  final Color textColorMore;
  final EdgeInsetsGeometry padding;
  

  SectionRichTextView(
    this.title, {
    Key key,
    this.onPressed,
    this.more: " *",
    this.leftTextSize: 12.0,
    this.hiddenMore: false,
    this.textColor = ColorsHelper.TRUX_GREY_4_COLOR,
    this.textColorMore,
    this.padding,
  })  : assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
            padding: padding ?? EdgeInsets.fromLTRB(3, 1, 3, 2),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 2, top: 5,right: 5, bottom: 3),
                      child:  RichText(
              // overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.start,
              text: TextSpan(
                children: [
                  TextSpan(
                      text: title,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: textColor ,
                          fontSize: leftTextSize)),
                  TextSpan(
                      text: more,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: textColorMore ?? ColorsHelper.COLOR_RED_DOT_DARK,
                          fontSize: leftTextSize)),
                ],
              ),
            ),
                      // decoration: UnderlineTabIndicator(
                      //     borderSide:
                      //         BorderSide(width: 2.0, color: Colors.white),
                      //     insets: EdgeInsets.fromLTRB(0, 10, 0, 0))
                          ),
             
                ])),
      ],
    );
  }
}
