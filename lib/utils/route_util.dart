
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void pushNewPageBack(BuildContext context, Widget routePage,
    {Function callBack}) {
  Navigator.of(context)
      .push(CupertinoPageRoute(builder: (context) => routePage))
      .then((data) {
    if (data != null) {
      callBack(data);
    }
  });
}