class UIStringHelper {
static final String login = 'Login';
static final String signInLower = 'Sign In';
static final String signUpLower = 'Sign up';
static final String account = 'Don\'t have an account?';
static final String createAccount = 'Create your account.';
static final String emailId = 'Email Id';
static final String password = 'Password';
static final String remember = 'Remember Me';
static final String forgotPassword = 'Forgot password?';
static final String loginWith = 'or login with';
static final String facebook = 'facebook';
static final String googlePlus = 'google-plus';
static final String twitter = 'twitter';

static final String introduction_screen_catch_phrase_title_1 = 'Rent truck, cover van at the lowest price for any of your needs! ';
static final String introduction_screen_catch_phrase_text = 'By using Trux24 app you can get any types of truck, covered van for your trips or give your truck for rent. ';
static final String introStart = "Let's Start";

// Login 
static final String pleaseGiveMobileNumber = 'Please give your mobile number';
static final String loginParagraphTermsAndConditions = 'By clicking on "Log in with Mobile" you confirm that you accept the ';
static final String loginParagraphTermsAndConditionsUnderline = 'Trux24 Terms and Conditions';
static final String btnContinue = 'Continue';
static final String trux24TermsConsitionlink = 'https://trux24.com/terms_conditions';
 
// OTP Verify
static final String verificationCodeLabel = 'Verification Code';
static final String pleaseGiveYourOtpNumber = 'Please enter 4 digit verification code that has been sent to +8801400524535';

static final String otpVerifyWaitingMessage = 'You can resend verification code after';

static final String dashboardScreenTitle = 'Dashboard';
static final String moreOptionProfileLabel = 'Profile';
static final String moreOptionPaymentHistoryLabel = 'Payment history';
static final String moreOptionPaymentMethodsLabel = 'Payment methods';
static final String moreOptionPromotionsLabel = 'Promotions';
static final String moreOptionReviewsabel = 'Reviews';
static final String moreOptionNotificationsLabel = 'Notifications';
static final String moreOptionTaskAlertsLabel = 'Task alerts Setting';
static final String moreOptionSettingsLabel = 'Settings';
static final String moreOptionHelpLabel = 'Help';
static final String moreOptionAdminLabel = 'Admin';
static final String moreOptionMyBalanceLabel = 'My Balance';
static final String moreOptionLogoutLabel = 'Logout';


   
// Post Task Details
static final String postTaskDetailsTaskareYouSureLabelNO = 'If you are not sure what type of truck you will select, please press the No button.';
static final String postTaskDetailsTaskareYouSureLabelYES = 'If you are sure what type of truck you will select, please press the Yes button.';


static final String postTaskDetailsTruckTypeLabel = 'TRUCK TYPE';
static final String postTaskDetailsTruckCapacityLabel = 'TRUCK CAPACITY';
static final String postTaskDetailsTruckSizeLabel = 'TRUCK SIZE';
static final String postTaskDetailsDescripbeTripLabel = 'DESCRIBE YOUR TRIP';
static final String postTaskDetailsDescriptionHint = 'Please describe what type of product you want to transport...';
static final String postTaskDetailsSelectYourGoodsHint = 'Select your goods';
static final String postTaskDetailsSelectGoodsTypeLabel = 'GOOD TYPES';
static final String postTaskDetailsSelectGoodsLabel = 'Goods';
// Post Task Due Date
static final String postTaskDueDateLocationLabel = 'Location';
static final String postTaskDueDatePickUpLocationLabel = 'Pick up location';
static final String postTaskDueDateDropOffLocationLabel = 'Drop off location';

static final String postTaskDueDateSelectPickUpLocationLabel = 'Select your pick up location';
static final String postTaskDueDateSelectDropOffLocationLabel = 'Select your drop up location';
static final String postTaskDueDatePriceBidDurationLabel = 'Offer Duration (minutes),  e.g 120 min';
static final String postTaskDueDateScheduleLabel = 'When do you need it done?';
// Post Task Trip Budget
static final String postTaskBudgetLabel = "What's your budget";
static final String postTaskBudgetEstimatedLabel = 'Estimated Budget:';
static final String postTaskBudgetUploadImagesLabel = 'Upload Goods Images';
static final String postTaskBudgetTruckTaskersLabel = 'How many Truck do you need?';
static final String postTaskBudgetLaborTaskersLabel = 'How many Labor do you need?';

// Task Details
static final String postTaskDetailsLabel = 'Details';
static final String postTaskDetailsPostByLabel = 'POSTED BY';
static final String postTaskDetailsTripIdLabel = 'TRIP ID #';
static final String postTaskDetailsNowLabel = 'Now';
static final String postTaskDetailsNoOfTrucksLabel = 'NO OF TRUCKS:';
static final String postTaskDetailsRescheduleLabel = 'Reschedule';
static final String postTaskDetailsDateLabel= 'Date';
static final String postTaskDetailsTimeLabel = 'Time';
static final String postTaskDetailsHourLabel= 'Hours';
static final String postTaskDetailsMinLabel = 'Minutes';
static final String postTaskDetailsSecondsLabel = 'Seconds';
static final String postTaskDetailsMakeOffersLabel = 'Make Offer';
static final String postTaskDetailsTkLabel = '৳';
static final String postTaskDetailsTimeLeftLabel = 'TIME LEFT';
static final String postTaskDetailsCallForHelpLabel = 'Call for help';
static final String postTaskDetailsOffersLabel = 'OFFERS';
static final String postTaskDetailsStatusActive = 'Active';

}