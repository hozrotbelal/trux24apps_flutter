


const String title = 'Login Page';
const String login = 'Login';
const String signInLower = 'Sign In';
const String signUpLower = 'Sign up';
const String account = 'Don\'t have an account?';
const String createAccount = 'Create your account.';
const String emailId = 'Email Id';
const String userName = 'UserName';
const String password = 'Password';
const String remember = 'Remember Me';
const String forgotPassword = 'Forgot password?';
const String loginWith = 'or login with';
const String facebook = 'facebook';
const String googlePlus = 'google-plus';
const String twitter = 'twitter';
const String detailsLower = 'Details';
