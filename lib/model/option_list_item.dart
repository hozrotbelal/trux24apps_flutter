import 'package:flutter/material.dart';



class OptionListItem {

  final String title;
  final String notification;

  const  OptionListItem(
      this.title,
      this.notification);
  
}