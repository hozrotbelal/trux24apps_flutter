import 'package:flutter/material.dart';



class Review {

  final int id;
  final int userId;

  const  Review(
      this.id,
      this.userId);
  
}