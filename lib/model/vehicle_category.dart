import 'package:flutter/material.dart';



class VehicleCategory {

  final String iconName;
  final String iconUrl;

  const  VehicleCategory(
      this.iconName,
      this.iconUrl);
  
}