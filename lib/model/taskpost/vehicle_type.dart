import 'package:flutter/material.dart';



class VehicleType {

  final int id;
  final String name;
  final String iconUrl;

  const  VehicleType(
      this.id,
      this.name,
      this.iconUrl);
  
}