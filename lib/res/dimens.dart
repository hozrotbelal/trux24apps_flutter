class Dimens {
  static const double font_sp10 = 10;
  static const double font_sp12 = 12;
  static const double font_sp14 = 14;
  static const double font_sp16 = 16;
  static const double font_sp18 = 18;
  static const double font_sp20 = 20;
  static const double font_sp26 = 26;
  static const double font_sp40 = 40;

  static const double gap_dp1 = 1;
  static const double gap_dp2 = 2;
  static const double gap_dp3 = 3;
  static const double gap_dp4 = 4;
  static const double gap_dp5 = 5;
  static const double gap_dp6 = 6;
  static const double gap_dp8 = 8;
  static const double gap_dp10 = 10;
  static const double gap_dp12 = 12;
  static const double gap_dp15 = 15;
  static const double gap_dp16 = 16;
  static const double gap_dp20 = 20;
  static const double gap_dp24 = 24;
  static const double gap_dp25 = 25;
  static const double gap_dp30 = 30;
  static const double gap_dp35 = 35;
  static const double gap_dp40 = 40;
  static const double gap_dp48 = 48;
  static const double gap_dp50 = 50;
  static const double gap_dp55 = 55;
  static const double gap_dp60 = 60;
  static const double gap_dp65 = 65;
  static const double gap_dp70 = 70;
  static const double gap_dp75 = 75;
  static const double gap_dp80 = 80;
  static const double gap_dp85 = 85;
  static const double gap_dp90 = 90;
  static const double gap_dp95 = 95;
  static const double gap_dp100 = 100;
  static const double gap_dp110 = 110;
  static const double gap_dp115 = 115;
  static const double gap_dp120 = 120;
  static const double gap_dp125 = 125;
  static const double gap_dp130 = 130;
  static const double gap_dp135 = 135;
  static const double gap_dp140 = 140;
  static const double gap_dp150 = 150;
  static const double gap_dp160 = 160;
  static const double gap_dp170 = 170;
  static const double gap_dp180 = 180;
  static const double gap_dp190 = 190;
  static const double gap_dp200 = 200;
  static const double gap_dp210 = 210;
  static const double gap_dp220 = 220;
  static const double gap_dp230 = 230;

  static const double line_dp2 = 2;
  static const double line_dp1 = 1;
  static const double line_dp_half = 0.5;

  static const double homeImageSize = 27.0;

  static const double maxFontSize = 30.0;
  static const double minFontSize = 10.0;

  static const double minSpace = 1.0;
  static const double maxSpace = 3.0;

  static const double chapterItemHeight = 50.0;
}
