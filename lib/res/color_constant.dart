import 'package:flutter/material.dart';

class ColorsHelper {
  // Color primary
  static const Color PRIMARY_COLOR_DARK = Color(0xFFf0873c);
  static const Color PRIMARY_COLOR = Color(0xFFF68E47);
  static const Color TRUX_GREY_DARK_COLOR = Color(0xFF3d3c59);
  static const Color TRUX_GREY_DARK_LIGHT_COLOR = Color(0xFF646464);
  static const Color TRUX_GREY_SHOHOKAEY_COLOR = Color(0xFFb2b4b6);

 
  static const Color TRUX_GREY_1_COLOR = Color(0xFFf5f9fb);
  static const Color TRUX_GREY_2_COLOR = Color(0xFFe7eef1);
  static const Color TRUX_GREY_3_COLOR = Color(0xFFcad7dc);
  static const Color TRUX_GREY_4_COLOR = Color(0xFF839094);
  static const Color TRUX_GREY_5_COLOR = Color(0xFF3a3d3e);


  // Task details timer
  static const Color COUNT_DOWN_HOURS_TRANSPARENT_COLOR = Color(0x20008fb4);
  static const Color COUNT_DOWN_MIN_TRANSPARENT_COLOR = Color(0x157632A7);
  static const Color COUNT_DOWN_SECOND_TRANSPARENT_COLOR = Color(0x259fc104);

  static const Color TRUX_GREY_TRANSPARENT_COLOR = Color(0xa6efefef);
  static const Color AIR_BLUE_COLOR = Color(0xFF008fb4);
  static const Color AIR_RED_COLOR = Color(0xFFec514b);
  static const Color AIR_ORRANGE_COLOR = Color(0xFFf5ab5d);
  static const Color AIR_ORRANGE_LIGHT_COLOR = Color(0xFFf5cd5d);
  static const Color AIR_PINK_COLOR = Color(0xFFfb675b);
  static const Color AIR_PINK_PRESSED_COLOR = Color(0xFFe5695f);
  static const Color AIR_GREEN_COLOR = Color(0xFF9fc104);
  static const Color TASK_STATE_GREEN_DARK_COLOR = Color(0xFF38be55);
  static const Color GREEN_COLOR = Color(0xFF388E3C);
  static const Color TASK_STATE_TEAL_COLOR = Color(0xFF0CD497);
  static const Color TASK_STATE_TEAL_DARK_COLOR = Color(0xFF004d40);
  static const Color TASK_STATE_CYAN_COLOR = Color(0xFF7632A7);
  static const Color TASK_STATE_BLUE_DARK_COLOR = Color(0xFF1565C0);
  static const Color TASK_STATE_YELLOW_COLOR = Color(0xFFf4be36);

// Status background
  static const Color ROUND_SHAPE_TASK_STATUS_GREEN_COLOR = Color(0x259fc104);
  static const Color ROUND_SHAPE_TASK_STATUS_PURPLE_COLOR = Color(0x157632A7);
  static const Color ROUND_SHAPE_TASK_STATUS_BLUE_COLOR = Color(0x20008fb4);
  static const Color ROUND_SHAPE_TASK_STATUS_CANCEL_RED_COLOR =
      Color(0x15EE1C25);
  static const Color ROUND_SHAPE_TASK_STATUS_DRAFT_GREY_COLOR =
      Color(0x20839094);
  static const Color ROUND_SHAPE_TASK_STATUS_TEAL_COLOR = Color(0x250CD497);

  // Grey different color-->
  static const Color COLOR_GREY_100 = Color(0xFFF5F5F5);
  static const Color COLOR_GREY_200 = Color(0xFFEEEEEE);
  static const Color COLOR_GREY_300 = Color(0xFFE0E0E0);
  static const Color COLOR_GREY_400 = Color(0xFFBDBDBD);
  static const Color COLOR_GREY_500 = Color(0xFF9E9E9E);
  static const Color COLOR_GREY_600 = Color(0xFF757575);
  static const Color COLOR_GREY_700 = Color(0xFF616161);
  static const Color COLOR_GREY_800 = Color(0xFF424242);
  static const Color COLOR_GREY_900 = Color(0xFF212121);
  static const Color COLOR_BLACK = Color(0xFF000000);

  static const Color COLOR_GREY_CCOLOR = const Color(0xFFCCCCCC);
  static const Color COLOR_GREY_3COLOR = const Color(0xFF333333);
  static const Color COLOR_GREY_6COLOR = const Color(0xFF666666);
  static const Color COLOR_GREY_9COLOR = const Color(0xFF999999);

  // Blue different color-->
  static const Color COLOR_BLUE_50 = Color(0xFFE3F2FD);
  static const Color COLOR_BLUE_100 = Color(0xFFBBDEFB);
  static const Color COLOR_BLUE_200 = Color(0xFF90CAF9);
  static const Color COLOR_BLUE_300 = Color(0xFF64B5F6);
  static const Color COLOR_BLUE_400 = Color(0xFF42A5F5);
  static const Color COLOR_BLUE_500 = Color(0xFF2196F3);
  static const Color COLOR_BLUE_600 = Color(0xFF1E88E5);
  static const Color COLOR_BLUE_700 = Color(0xFF1976D2);
  static const Color COLOR_BLUE_800 = Color(0xFF1565C0);
  static const Color COLOR_BLUE_900 = Color(0xFF0D47A1);
  static const Color COLOR_BLUE_A100 = Color(0xFF82B1FF);
  static const Color COLOR_BLUE_A200 = Color(0xFF448AFF);
  static const Color COLOR_BLUE_A400 = Color(0xFF2979FF);
  static const Color COLOR_BLUE_A700 = Color(0xFF2962FF);

  // Transferent different color-->
  static const Color COLOR_TRANSFERENT = Color(0x00000000);
  static const Color COLOR_TRANSFERENT_5 = Color(0x0d000000);
  static const Color COLOR_TRANSFERENT_5_WHITE = Color(0x0dffffff);

  static const Color COLOR_TRANSFERENT_12 = Color(0x1e000000);
  static const Color COLOR_TRANSFERENT_25 = Color(0x3f000000);
  static const Color COLOR_TRANSFERENT_30 = Color(0x4c000000);
  static const Color COLOR_TRANSFERENT_37 = Color(0x5e000000);
  static const Color COLOR_TRANSFERENT_50 = Color(0x80000000);
  static const Color COLOR_TRANSFERENT_62 = Color(0x9e000000);
  static const Color COLOR_TRANSFERENT_75 = Color(0xbf000000);
  static const Color COLOR_TRANSFERENT_87 = Color(0xdd000000);

  // Red different color-->
  static const Color COLOR_RED_50 = Color(0xFFFFEBEE);
  static const Color COLOR_RED_100 = Color(0xFFFFCDD2);
  static const Color COLOR_RED_200 = Color(0xFFEF9A9A);
  static const Color COLOR_RED_300 = Color(0xFFE57373);
  static const Color COLOR_RED_400 = Color(0xFFEF5350);
  static const Color COLOR_RED_500 = Color(0xFFF44336);
  static const Color COLOR_RED_600 = Color(0xFFE53935);
  static const Color COLOR_RED_700 = Color(0xFFD32F2F);
  static const Color COLOR_RED_800 = Color(0xFFC62828);
  static const Color COLOR_RED_900 = Color(0xFFB71C1C);
  static const Color COLOR_RED_A100 = Color(0xFFFF8A80);
  static const Color COLOR_RED_A200 = Color(0xFFFF5252);
  static const Color COLOR_RED_A400 = Color(0xFFFF1744);
  static const Color COLOR_RED_A700 = Color(0xFFD50000);
  static const Color COLOR_RED_DOT_DARK = Color(0xFFd1395c);

  // Orange different color-->
  static const Color COLOR_DEEP_ORANGE_50 = Color(0xFFFBE9E7);
  static const Color COLOR_DEEP_ORANGE_100 = Color(0xFFFFCCBC);
  static const Color COLOR_DEEP_ORANGE_200 = Color(0xFFFFAB91);
  static const Color COLOR_DEEP_ORANGE_300 = Color(0xFFFF8A65);
  static const Color COLOR_DEEP_ORANGE_400 = Color(0xFFFF7043);
  static const Color COLOR_DEEP_ORANGE_500 = Color(0xFFFF5722);
  static const Color COLOR_DEEP_ORANGE_600 = Color(0xFFF4511E);
  static const Color COLOR_DEEP_ORANGE_700 = Color(0xFFE64A19);
  static const Color COLOR_DEEP_ORANGE_800 = Color(0xFFD84315);
  static const Color COLOR_DEEP_ORANGE_900 = Color(0xFFBF360C);
  static const Color COLOR_DEEP_ORANGE_A100 = Color(0xFFFF9E80);
  static const Color COLOR_DEEP_ORANGE_A200 = Color(0xFFFF6E40);
  static const Color COLOR_DEEP_ORANGE_A400 = Color(0xFFFF3D00);
  static const Color COLOR_DEEP_ORANGE_A700 = Color(0xFFDD2C00);

  // Common colors
  static const Color WHITE = Colors.white;
  static const Color BLACK = Colors.black;
  static const Color COLOR_BG = Color(0xFF4A6572);

  static const Color COLOR_NEARLY_BLUE = Color(0xFF00B6F0);
  static const Color COLOR_NEARLY_BLACK = Color(0xFF213333);
  static const Color COLOR_GREY = Color(0xFF3A5160);
  static const Color COLOR_DARK_GREY = Color(0xFF313A44);

  static const Color COLOR_DARK_TEXT = Color(0xFF253840);
  static const Color COLOR_DARKET_TEXT = Color(0xFF17262A);
  static const Color COLOR_ONLINE_GREEN = Color(0xFF4CBB17);
//StepColor
  static const stepCompleteColor = Color(0xFFf0873c);
  static const stepEdit =  Color(0xFFb2b4b6);
  static const stepBackground = Color.fromRGBO(246, 249, 255, 1);
  
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
