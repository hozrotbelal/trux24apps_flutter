import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/utils/ui_helper.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget(
      {Key key,
      @required this.onPressed,
      this.color,
      this.text,
      this.shadow,
      this.borderSideColor,
      this.topPadding,
      this.padding,
      this.boxHeight,
      this.circleAvatar = false,
      this.rightWidget,
      this.textColor})
      : super(key: key);

  final Color color;
  final String text;
  final Color shadow;
  final Color borderSideColor;
  final double topPadding;
  final double boxHeight;
  final Color textColor;
  final bool circleAvatar;
  final Widget rightWidget;
  final EdgeInsetsGeometry padding;

  final GestureTapCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding:  EdgeInsets.fromLTRB(10, topPadding, 10, 0),
      child: RaisedButton(
          color: color,
          shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
          side: BorderSide(color: borderSideColor ?? ColorsHelper.PRIMARY_COLOR)),
          onPressed: onPressed,
          child: SizedBox(
            height: boxHeight ?? 50,
            width: double.infinity,
            child: Container(
              padding: padding ?? EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Row(children: <Widget>[
                Expanded(
                  child: Text(text,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: textColor ?? UIHelper.BLUEBERRY_GREY,
                          fontWeight: FontWeight.bold)),
                ),
                if (circleAvatar)
                  (CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Icon(Icons.chevron_right, color: ColorsHelper.PRIMARY_COLOR),
                  ))
                  else
                  rightWidget ?? Text('')
                // child: RaisedButton(
                //   color: color,
                //   shape: RoundedRectangleBorder(
                //       borderRadius: new BorderRadius.circular(30.0)),
                //   onPressed: () {},
                //   child: Text(
                //     text,
                //     style: TextStyle(
                //         fontSize: 20, color: UIHelper.SPOTIFY_TEXT_COLOR),
                //   ),
                // ),
              ]),
            ),
          )),
    ));
  }
}
