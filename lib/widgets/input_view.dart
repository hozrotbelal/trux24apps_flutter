import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trux24/res/custom_icon.dart';
import 'package:trux24/res/styles.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final int maxLength;
  final int minLines;
  final int maxLines;
  final bool autoFocus;
  final TextInputType keyboardType;
  final String hintText;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final bool isInputPwd;
  final Function getVCode;
  final Duration duration;
  final Widget prefixIcon;
  final TextStyle hintTextStyle;
  final bool etEnabled; 
  final InputDecoration decoration;
  final TextAlign textAlign;
  final TextAlignVertical textAlignVertical;
  final bool isShowIconDelete;        

  CustomTextField({
    Key key,
    @required this.controller,
    this.maxLength = 10,
    this.maxLines = 1,
    this.minLines,
    this.autoFocus: false,
    this.keyboardType: TextInputType.text,
    this.hintText: "",
    this.focusNode,
    this.nextFocusNode,
    this.isInputPwd: false,
    this.getVCode,
    this.prefixIcon,
    this.hintTextStyle,
    this.etEnabled : true,
    this.decoration,
    this.textAlign = TextAlign.left,
    this.textAlignVertical ,
    this.isShowIconDelete : true,
    this.duration: const Duration(seconds: 60),
  }) :assert(maxLines == null || maxLines > 0),
       assert(minLines == null || minLines > 0),
       assert(
         (maxLines == null) || (minLines == null) || (maxLines >= minLines),
         "minLines can't be greater than maxLines",
       ), super(key: key);

  @override
  createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isShowPwd = false;
  bool _isShowDelete = true;

  @override
  void initState() {
    super.initState();

    //Monitor input changes
    widget.controller.addListener(() {
      setState(() {
        _isShowDelete = widget.controller.text.isEmpty;
      });
    });
  }

  @override
  void dispose() {
  
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerRight,
      children: <Widget>[
        TextField(
          focusNode: widget.focusNode,
          maxLength: widget.maxLength,
          minLines : widget.minLines ,
          maxLines : widget.maxLines,
          textAlign: widget.textAlign ?? TextAlign.left,
          textAlignVertical: widget.textAlignVertical ?? TextAlignVertical.center,
          style: TextStyle(textBaseline: TextBaseline.alphabetic),

          /// Code executed after the keyboard action button is clicked: The cursor switches to the specified input box
          onEditingComplete: widget.nextFocusNode == null
              ? null
              : () => FocusScope.of(context).requestFocus(widget.nextFocusNode),
          obscureText: widget.isInputPwd ? !_isShowPwd : false,
          autofocus: widget.autoFocus,
          controller: widget.controller,
          textInputAction: TextInputAction.done,
          keyboardType: widget.keyboardType,
          //Number and mobile phone number restriction format is 0 to 9 (white list), password restrictions do not include Chinese characters (black list)
          inputFormatters: (widget.keyboardType == TextInputType.number ||
                  widget.keyboardType == TextInputType.phone)
              ? [WhitelistingTextInputFormatter(RegExp("[0-9]"))]
              : [BlacklistingTextInputFormatter(RegExp("[\u4e00-\u9fa5]"))],
          decoration: widget.decoration ?? InputDecoration(
            contentPadding: const EdgeInsets.only(left: 2, top: 0,right: 4),
            hintText: widget.hintText,
            hintStyle: widget.hintTextStyle ?? TextStyles.textGreyC14,
            counterText: "",
            border: InputBorder.none,
            // focusedBorder: UnderlineInputBorder(
            //     borderSide: BorderSide(color: Colors.blueAccent, width: 0.8)),
            // enabledBorder: UnderlineInputBorder(
            //     borderSide: BorderSide(color: Color(0xFFEEEEEE), width: 0.8)),
            prefixIcon: widget.prefixIcon,
               
          ),
          enabled: widget.etEnabled,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
                visible: !_isShowDelete,
                child: InkWell(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    child: Visibility( visible: widget.isShowIconDelete,
                    child: Icon(Icons.close, size: 18.0)),
                    onTap: () => setState(() => widget.controller.text = ""))),
            Visibility(
                visible: widget.isInputPwd,
                child: Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: InkWell(
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        child: Icon(
                            _isShowPwd
                                ? CustomIcon.show_password
                                : CustomIcon.hidden_password,
                            size: 18.0),
                        onTap: () {
                          setState(() => _isShowPwd = !_isShowPwd);
                        }))),
           
          ],
        )
      ],
    );
  }
}