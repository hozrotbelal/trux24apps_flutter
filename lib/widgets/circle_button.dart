import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:flutter/widgets.dart';
// ignore: implementation_imports
import 'package:flutter_vector_icons/src/font_awesome.dart';

class CircleButton extends StatelessWidget {
  final IconData icon;
  final double iconSize;
  final double size;
  final EdgeInsetsGeometry padding;
  final void Function() onPressedAction;
  final double elevation;
  final double highlightElevation;
  final Color fillColor;
  final Color splashColor;
  final Color highlightColor;
  final Color iconColor;
  final Widget widget;
  const CircleButton(
      {Key key,
      @required this.widget,
      this.icon,
      this.onPressedAction,
      this.iconSize: 20,
      this.size: 50,
      this.padding = const EdgeInsets.all(0.0),
      this.fillColor: Colors.white,
      this.splashColor,
      this.highlightColor,
      this.elevation: 10.0,
      this.highlightElevation: 5.0,
      this.iconColor,
    })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
       child:  SizedBox(
        width: size,
        height: size,
        child: RawMaterialButton(
          fillColor: fillColor,
          splashColor: splashColor,
          highlightColor: highlightColor,
          elevation: elevation,
          highlightElevation: highlightElevation,
          onPressed: onPressedAction,
          child: widget ?? Icon(
                     icon??   FontAwesome.male,
                    color: ColorsHelper.WHITE ?? Theme.of(context).accentColor,
                    size: iconSize ?? 20,
                  ),
          shape: CircleBorder(),
        ),
      ),
     
    );
  }
}
