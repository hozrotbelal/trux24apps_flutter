import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';


class LightTheme extends StatelessWidget {
  final Widget child;

  LightTheme({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            accentColor: ColorsHelper.PRIMARY_COLOR,
            primaryColor: ColorsHelper.PRIMARY_COLOR,
            scaffoldBackgroundColor: Colors.grey[200],
            appBarTheme: AppBarTheme(
                iconTheme: lightIconTheme,
                elevation: 0.0,
                brightness: Brightness.light,
                color: Colors.white)),
        child: child);
  }
}
