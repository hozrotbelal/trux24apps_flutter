import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/enum/enum.dart';
import 'package:trux24/model/taskpost/vehicle_type.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/details/task_details_coundown_view.dart';
import 'package:trux24/ui_widget/details/task_details_quick_action_view.dart';

import 'package:trux24/ui_widget/details/task_details_date_view.dart';
import 'package:trux24/ui_widget/details/task_details_posted_by.dart';
import 'package:trux24/ui_widget/details/task_details_offer_view.dart';
import 'package:trux24/ui_widget/section/section_richtext_view.dart';
import 'package:trux24/ui_widget/section/section_view.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_vector_icons/src/font_awesome.dart';
import 'package:trux24/widgets/line.dart';

class TaskDetailsScreen extends StatefulWidget {
  @override
  _TaskDetailsUI createState() => _TaskDetailsUI();
}

class _TaskDetailsUI extends State<TaskDetailsScreen> {
  List<VehicleType> mVehicleTypeList = new List();
  List<VehicleType> mVehicleCapacityList = new List();
  List<VehicleType> mVehicleSizeList = new List();
  bool values = false;
  String txtYesOrNo = UIStringHelper.postTaskDetailsTaskareYouSureLabelNO;
  TextEditingController _goodsTypeController = TextEditingController();
  TextEditingController _describeController = TextEditingController();

  final FocusNode _goodsTypePhone = FocusNode();
  final FocusNode _nodeDescribe = FocusNode();
  int _selectedCat = 0;

  @override
  void initState() {
    super.initState();
    mVehicleTypeList.add(new VehicleType(0, "All", ""));
    mVehicleTypeList.add(new VehicleType(1, "Open Truck", ""));
    mVehicleTypeList.add(new VehicleType(2, "Covered Van", ""));

    print(mVehicleTypeList);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Trip Details'),
          elevation: 0,
          backgroundColor: ColorsHelper.PRIMARY_COLOR,
//            leading: IconButton(
//                icon: Icon(
//                  Icons.list,
//                  color: Colors.white,
//                ),
//                onPressed: () {}),
        ),
        body: Container(
            margin: const EdgeInsets.only(left: 7, right: 7, top: 5, bottom: 5),
            child: ListView(shrinkWrap: true, children: <Widget>[
              Column(
                  // center the children
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                     SectionRichTextView(
                          "Heavy truck (Open), 12 feet, 10 Ton",
                          onPressed: () {},
                          more: "",
                          textColor: ColorsHelper.TRUX_GREY_4_COLOR,
                          textColorMore :ColorsHelper.TRUX_GREY_4_COLOR ,
                          leftTextSize: 20,
                          padding: EdgeInsets.all(1),
                        ),
                    Gaps.vGap4,
                    DetailsPostedByTask(circleButtonSize: 50.0),
                    Gaps.vGap5,
                    TaskDetailsDateView(
                      onPressed: () {},
                      iconWidget: SvgPicture.asset(
                        "assets/images/vector_ic_truck.svg",
                        color: ColorsHelper.TRUX_GREY_4_COLOR,
                        height: 15,
                        width: 15,
                      ),
                      // iconUrl : "assets/images/ic_taka_12.png",
                      iconColor: ColorsHelper.TRUX_GREY_4_COLOR,
                      iconSize: 20,
                      widget: Container(
                          padding: EdgeInsets.all(5.0),
                          width: double.infinity,
                          child: Expanded(
                            child: Row(
                              children: <Widget>[
                                Text(
                                    UIStringHelper
                                        .postTaskDetailsNoOfTrucksLabel,
                                    style: TextStyle(
                                      color: ColorsHelper.TRUX_GREY_4_COLOR,
                                      fontSize: 13,
                                    )),
                                Gaps.hGap10,
                                Text(
                                  "1 trucks",
                                  style: TextStyle(
                                    color: ColorsHelper.TRUX_GREY_5_COLOR,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),

                    TaskDetailsDateView(
                      onPressed: () {},
                      iconWidget: SvgPicture.asset(
                        "assets/images/vector_ic_location.svg",
                        color: ColorsHelper.TRUX_GREY_4_COLOR,
                        height: 15,
                        width: 15,
                      ),

                      // iconUrl : "assets/images/ic_taka_12.png",
                      iconColor: ColorsHelper.TRUX_GREY_4_COLOR,
                      iconSize: 20,
                      widget: Container(
                          padding: EdgeInsets.all(5.0),
                          width: double.infinity,
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                          UIStringHelper
                                              .postTaskDetailsDateLabel
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color:
                                                ColorsHelper.TRUX_GREY_4_COLOR,
                                            fontSize: 13,
                                          )),
                                      Gaps.vGap4,
                                      Text("Sat, Feb 29, 2020 ",
                                          style: TextStyle(
                                            color:
                                                ColorsHelper.TRUX_GREY_5_COLOR,
                                            fontSize: 13,
                                          )),
                                    ]),
                              ],
                            ),
                          )),
                    ),

                    TaskDetailsDateView(
                      onPressed: () {},
                      iconWidget: SvgPicture.asset(
                        "assets/images/vector_ic_location.svg",
                        color: ColorsHelper.TRUX_GREY_4_COLOR,
                        height: 15,
                        width: 15,
                      ),
                      iconColor: ColorsHelper.TRUX_GREY_4_COLOR,
                      iconSize: 20,
                      widget: Container(
                          padding: EdgeInsets.all(5.0),
                          width: double.infinity,
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                      Text(
                                          UIStringHelper
                                              .postTaskDetailsTimeLabel
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color:
                                                ColorsHelper.TRUX_GREY_4_COLOR,
                                            fontSize: 13,
                                          )),
                                      Gaps.vGap4,
                                      Text("08:10 Am",
                                          style: TextStyle(
                                            color:
                                                ColorsHelper.TRUX_GREY_5_COLOR,
                                            fontSize: 13,
                                          )),
                                    ])),
                                Gaps.hGap10,
                                Container(
                                  margin: const EdgeInsets.only(
                                      left: 0, right: 5, top: 5, bottom: 0),
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    UIStringHelper
                                        .postTaskDetailsRescheduleLabel,
                                    style: TextStyle(
                                      color: ColorsHelper.PRIMARY_COLOR,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),
                    Gaps.vGap20,
                    _buildTextView(),
                    Gaps.vGap5,
                    // Timer View
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            child: DetailsCountDownTask(
                          textColor: ColorsHelper.AIR_BLUE_COLOR,
                          bgColor:
                              ColorsHelper.COUNT_DOWN_HOURS_TRANSPARENT_COLOR,
                          boxText: "12",
                          boxTextFontSize: 30,
                          bottomText: UIStringHelper.postTaskDetailsHourLabel,
                          bottomTextFontSize: 15,
                          boxSizeHeight: 55.0,
                          boxSizeWidth: 65.0,
                          boxBorderRadious: 5.0,
                          boxPadding: EdgeInsets.fromLTRB(12, 10, 12, 10),
                          widget: Text("00" ?? "",
                              style: TextStyle(
                                  color: ColorsHelper.AIR_BLUE_COLOR,
                                  fontSize: 30)),
                        )),
                        Container(
                            child: DetailsCountDownTask(
                          textColor: ColorsHelper.TASK_STATE_CYAN_COLOR,
                          bgColor:
                              ColorsHelper.COUNT_DOWN_MIN_TRANSPARENT_COLOR,
                          boxText: "12",
                          boxTextFontSize: 30,
                          bottomText: UIStringHelper.postTaskDetailsMinLabel,
                          bottomTextFontSize: 15,
                          boxSizeHeight: 55.0,
                          boxSizeWidth: 65.0,
                          boxBorderRadious: 5.0,
                          boxPadding: EdgeInsets.fromLTRB(12, 10, 12, 10),
                          widget: Text("00" ?? "",
                              style: TextStyle(
                                  color: ColorsHelper.TASK_STATE_CYAN_COLOR,
                                  fontSize: 30)),
                        )),
                        Container(
                            child: DetailsCountDownTask(
                          textColor: ColorsHelper.TASK_STATE_GREEN_DARK_COLOR,
                          bgColor:
                              ColorsHelper.COUNT_DOWN_SECOND_TRANSPARENT_COLOR,
                          boxText: "00",
                          boxTextFontSize: 30,
                          bottomText:
                              UIStringHelper.postTaskDetailsSecondsLabel,
                          bottomTextFontSize: 15,
                          boxSizeHeight: 55.0,
                          boxSizeWidth: 65.0,
                          boxBorderRadious: 5.0,
                          boxPadding: EdgeInsets.fromLTRB(12, 10, 12, 10),
                          widget: Text("00" ?? "",
                              style: TextStyle(
                                  color:
                                      ColorsHelper.TASK_STATE_GREEN_DARK_COLOR,
                                  fontSize: 30)),
                        )),
                      ],
                    ),
                    Gaps.vGap20,
                    //  Make Offer View
                    SectionView(
                        UIStringHelper.postTaskBudgetEstimatedLabel
                            .toUpperCase(),
                        onPressed: () {},
                        hiddenMore: true,
                        headerBgTopColor: ColorsHelper.TRUX_GREY_1_COLOR,
                        textLeftHeaderColor: ColorsHelper.PRIMARY_COLOR,
                        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1.5,
                            color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(15 ?? 0.0),
                            //                 <--- border radius here
                          ),
                        ),
                        borderRadius: 15.0,
                        leftWidgetHeader: Icon(
                          Icons.chevron_right,
                          color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                          size: 18,
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                //padding: EdgeInsets.all(3),
                                margin: const EdgeInsets.only(
                                    left: 7, right: 7, top: 15, bottom: 5),
                                child: RichText(
                                  // overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.start,
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                          text: UIStringHelper
                                              .postTaskDetailsTkLabel,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: ColorsHelper
                                                  .TRUX_GREY_5_COLOR,
                                              fontSize: 45)),
                                      TextSpan(
                                          text: '333333',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: ColorsHelper
                                                  .TRUX_GREY_5_COLOR,
                                              fontSize: 45)),
                                    ],
                                  ),
                                ),
                              ),
                              _buildMakeOfferButtonView(),
                              Gaps.vGap6,
                              _buildHelpButtonView(),
                              Gaps.vGap10,
                            ])),
                    Gaps.vGap20,

                    _buildActionCircularView(),
                    Container(
                        //padding: EdgeInsets.all(3),
                        margin: const EdgeInsets.only(
                            left: 0, right: 5, top: 0, bottom: 0),
                        child: SectionRichTextView(
                          UIStringHelper.postTaskDetailsLabel,
                          leftTextSize: 18,
                          onPressed: () {},
                          more: " ",
                          textColor: ColorsHelper.TRUX_GREY_4_COLOR,
                          padding: EdgeInsets.all(1),
                        )),

                    Container(
                        //padding: EdgeInsets.all(3),
                        margin: const EdgeInsets.only(
                            left: 5, right: 5, top: 0, bottom: 0),
                        child: Text("Hello World ",
                            style: TextStyle(
                              color: ColorsHelper.TRUX_GREY_5_COLOR,
                              fontSize: 13,
                            ))),
                    Gaps.vGap10,
            SectionRichTextView(
                          UIStringHelper.postTaskDetailsOffersLabel,
                          onPressed: () {},
                          more: " (1)",
                          textColor: ColorsHelper.TRUX_GREY_4_COLOR,
                          textColorMore :ColorsHelper.TRUX_GREY_4_COLOR ,
                          padding: EdgeInsets.all(1),
                        ),
                         Gaps.vGap2,
                    DetailsOfferByTask(
                      boxPadding:
                          EdgeInsets.only(left: 1, top: 0, bottom: 0, right: 1),
                    ),
                  ]),
            ])));
  }

  Widget _buildActionCircularView() {
    return Container(
        alignment: Alignment.center,
        width: double.infinity,
        height: 130,
        child: ListView(scrollDirection: Axis.horizontal, children: <Widget>[
          Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          alignment: Alignment.center,
                          child: DetailsQuickActionTask(
                            textColor: ColorsHelper.AIR_BLUE_COLOR,
                            bgColor: ColorsHelper.TRUX_GREY_2_COLOR,
                            boxTextFontSize: 30,
                            bottomText: UIStringHelper.postTaskDetailsHourLabel,
                            bottomTextFontSize: 15,
                            boxPadding: EdgeInsets.all(3),
                            boxMargin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                            circleButtonSize: 55,
                            widget: Icon(
                              FontAwesome.male,
                              color: ColorsHelper.TRUX_GREY_DARK_COLOR ??
                                  Theme.of(context).accentColor,
                              size: 20,
                            ),
                            //  widget : Text("00" ?? "",
                            //  style: TextStyle(
                            //   color:  ColorsHelper.TASK_STATE_GREEN_DARK_COLOR, fontSize:30)),
                          )),
                      // Container(
                      //                             alignment: Alignment.center,

                      //     child: DetailsQuickActionTask(
                      //   textColor: ColorsHelper.TASK_STATE_CYAN_COLOR,
                      //   bgColor: ColorsHelper.TRUX_GREY_2_COLOR,
                      //   boxTextFontSize: 30,
                      //   bottomText: UIStringHelper.postTaskDetailsMinLabel,
                      //   bottomTextFontSize: 15,
                      //   boxPadding: EdgeInsets.all(3),
                      //   boxMargin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                      //   circleButtonSize: 55,
                      //   widget: Icon(
                      //     FontAwesome.male,
                      //     color: ColorsHelper.TRUX_GREY_DARK_COLOR ??
                      //         Theme.of(context).accentColor,
                      //     size: 20,
                      //   ),
                      // )),
                      // Container(
                      //                             alignment: Alignment.center,

                      //     child: DetailsQuickActionTask(
                      //   textColor: ColorsHelper.TASK_STATE_GREEN_DARK_COLOR,
                      //   bgColor: ColorsHelper.TRUX_GREY_2_COLOR,
                      //   boxTextFontSize: 30,
                      //   bottomText: UIStringHelper.postTaskDetailsSecondsLabel,
                      //   bottomTextFontSize: 15,
                      //   boxPadding: EdgeInsets.all(3),
                      //   boxMargin: EdgeInsets.fromLTRB(10, 0, 5, 0),
                      //   circleButtonSize: 55,
                      //   widget: Icon(
                      //     FontAwesome.male,
                      //     color: ColorsHelper.TRUX_GREY_DARK_COLOR ??
                      //         Theme.of(context).accentColor,
                      //     size: 20,
                      //   ),
                      // )),
                    ],
                  )
                ]),
          )
        ]));
  }

  Widget _buildMakeOfferButtonView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: ButtonWidget(
          color: ColorsHelper.PRIMARY_COLOR,
          shadow: UIHelper.MUZ_BUTTONSHADOW,
          text: UIStringHelper.postTaskDetailsMakeOffersLabel,
          topPadding: 7,
          circleAvatar: true,
          onPressed: () {
            // Navigator.pop(context);
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => MainMenuScreen()));

            print("Task Details");
          },
        ),
      ),
    );
  }

  Widget _buildHelpButtonView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: ButtonWidget(
          color: ColorsHelper.GREEN_COLOR,
          shadow: UIHelper.MUZ_BUTTONSHADOW,
          borderSideColor: ColorsHelper.GREEN_COLOR,
          text: UIStringHelper.postTaskDetailsCallForHelpLabel,
          topPadding: 7,
          circleAvatar: false,
          rightWidget: Row(children: <Widget>[
            LineWidget(
                lineType: LineType.vertical,
                color: ColorsHelper.WHITE,
                height: 55.0,
                width: 1.0),
            Gaps.hGap12,
            Icon(Icons.call, color: ColorsHelper.WHITE),
          ]),
          onPressed: () {
            // Navigator.pop(context);
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => MainMenuScreen()));

            print("Task Details");
          },
        ),
      ),
    );
  }

  Widget _buildTextView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: Text(
          UIStringHelper.postTaskDetailsTimeLeftLabel,
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
            color: ColorsHelper.TASK_STATE_TEAL_DARK_COLOR,
          ),
        ),
      ),
    );
  }
}
