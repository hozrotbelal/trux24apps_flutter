import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:trux24/enum/enum.dart';

import 'package:trux24/model/review.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/badgelist/list_item_badge.dart';
import 'package:trux24/ui_widget/reviewlist/list_item_review.dart';
import 'package:trux24/utils/utils.dart';
import 'package:trux24/widgets/avatorglow/avatar_glow.dart';
import 'package:trux24/widgets/loader/loader.dart';
import 'package:trux24/widgets/ratingbar/smooth_star_rating.dart';
import 'package:trux24/widgets/segmented/segmented_control.dart';
import 'package:share/share.dart';
import 'package:trux24/widgets/theme/light_theme.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

//import 'package:material_segmented_control/material_segmented_control.dart';

class Profile2Screen extends StatefulWidget {
  @override
  _Profile2UI createState() => _Profile2UI();
}

class _Profile2UI extends State<Profile2Screen> {
  double navAlpha = 0;
  double headerHeight;
  ScrollController scrollController = ScrollController();

  Color c = ColorsHelper.PRIMARY_COLOR;
  final String mobileImg = "assets/bg_image/mobile_icon.png";
  List<Review> vReview = new List();
  int _currentSelection = 0;

  int sharedValue = 0;
  LoaderState _status = LoaderState.Loading;


  @override
  void initState() {
    super.initState();
    headerHeight = Utils.width;

    scrollController.addListener(() {
      var offset = scrollController.offset;
      if (offset < 0) {
        if (navAlpha != 0) {
          setState(() => navAlpha = 0);
        }
      } else if (offset < headerHeight) {
        if (headerHeight - offset <= Utils.navigationBarHeight) {
          setState(() => c = ColorsHelper.PRIMARY_COLOR);
        } else {
          c = ColorsHelper.PRIMARY_COLOR;
        }
        setState(() => navAlpha = 1 - (headerHeight - offset) / headerHeight);
      } else if (navAlpha != 1) {
        setState(() => navAlpha = 1);
      }
    });

    vReview.add(new Review(0, 100));
    vReview.add(new Review(1, 101));
    vReview.add(new Review(2, 102));
    vReview.add(new Review(3, 103));
    vReview.add(new Review(4, 104));
    vReview.add(new Review(5, 104));
    vReview.add(new Review(6, 104));
    _status = LoaderState.Succeed;
  }

  @override
  void dispose() {
    scrollController.dispose();
    // controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorsHelper.WHITE,
        body: Stack(children: <Widget>[
          LoaderContainer(
              contentView: EasyRefresh.custom(slivers: [
                      _buildSliverAppBar(),
      _builderContent(),
  _buildDetails(),
              ]),
         loaderState: _status),
        ]),
    );
  }

  Widget _buildBodyView() {
    return CustomScrollView(controller: scrollController, slivers: <Widget>[
      _buildSliverAppBar(),
      _builderContent(),
      _buildDetails(),
    ]);
  }

  Widget _buildSliverAppBar() {
    return SliverAppBar(
        // title: Text('${goods?.goodInfo?.goodsName}',
        backgroundColor: ColorsHelper.PRIMARY_COLOR,
        title: Text('Profile',
            style: TextStyle(color: ColorsHelper.WHITE, fontSize: 16)),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.edit), onPressed: () {}),
          IconButton(
              icon: Icon(Icons.share),
              onPressed: () {
                Share.share('Hello');
              }),
          Gaps.hGap5,
        ],
        pinned: true,
        expandedHeight: headerHeight - Utils.topSafeHeight,
        // leading: IconButton(
        //     icon: Icon(CustomIcon.back, color: c),
        //     onPressed: () => Navigator.pop(context)),
        // actions: <Widget>[
        //   IconButton(
        //       icon: Icon(Icons.shopping_cart, color: c),
        //      // onPressed: () => pushReplacementName(context, '/shopCart')
        //       )
        // ],
        flexibleSpace: FlexibleSpaceBar(
            background: Container(
                padding: EdgeInsets.only(top: Utils.navigationBarHeight),
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  ColorsHelper.PRIMARY_COLOR_DARK,
                  ColorsHelper.COLOR_TRANSFERENT_37
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                            margin: const EdgeInsets.fromLTRB(5, 3, 10, 0),
                            child: _circularShape),
                      ),
                      AvatarGlow(
                          startDelay: Duration(milliseconds: 1000),
                          glowColor: Colors.white,
                          endRadius: 70.0,
                          duration: Duration(milliseconds: 2000),
                          repeat: true,
                          showTwoGlows: true,
                          repeatPauseDuration: Duration(milliseconds: 100),
                          child: Material(
                              elevation: 8.0,
                              shape: CircleBorder(),
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Container(
                                      margin: const EdgeInsets.all(1.5),
                                      child: CircleAvatar(
                                        backgroundImage: NetworkImage(
                                            'https://images.pexels.com/photos/443356/pexels-photo-443356.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
                                        radius: 40.0,
                                        backgroundColor: ColorsHelper.WHITE,
                                      )),
                                  _circularShape
                                ],
                              ))),
                      Center(
                          child: Text(
                        'Hozrot Belal',
                        style:
                            TextStyle(fontSize: 15, color: ColorsHelper.WHITE),
                      )),
                      Gaps.vGap6,
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Container(
                                width: 17,
                                height: 17,
                                margin: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                decoration: BoxDecoration(
                                  color: ColorsHelper.COLOR_ONLINE_GREEN,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25)),
                                ),
                              ),
                            ),
                            TextSpan(
                                text: "Online",
                                style: TextStyle(
                                    color: ColorsHelper.WHITE, fontSize: 17)),
                          ],
                        ),
                      ),
                      Gaps.vGap6,
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Container(
                                  width: 17,
                                  height: 17,
                                  margin: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                  child: Icon(
                                    Icons.my_location,
                                    size: 16,
                                    color: ColorsHelper.WHITE,
                                  )),
                            ),
                            TextSpan(
                                text: "Bonossree, Dhaka",
                                style: TextStyle(
                                    color: ColorsHelper.WHITE, fontSize: 17)),
                          ],
                        ),
                      ),
                      Gaps.vGap6,
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Center(
                                child: Text("Member since 152 days ago",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: ColorsHelper
                                            .WHITE))), // 2nd wrap your widget in Center
                          ),
                          _textKamStatus,
                        ],
                      ),
                      Gaps.vGap4,
                    ]))));
  }

  Widget _builderContent() {
    return SliverToBoxAdapter(
        child: Container(
            padding: EdgeInsets.all(10.0),
            width: double.infinity,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Gaps.vGap12,
                  Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.fromLTRB(45, 0, 45, 5),
                      child: SegmentedWidget(
                        onChanged: (i) {
                          setState(() => _currentSelection =
                              _currentSelection == i ? null : i);
                        },
                        index: _currentSelection,
                        color: ColorsHelper.PRIMARY_COLOR,
                        textColor: ColorsHelper.WHITE,
                        borderRadius: 40,
                        children: [
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.all(3.0),
                                child: Text('As a Tasker')),
                          ),
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.all(3.0),
                                child: Text('As a Poster')),
                          )
                        ],
                      )),
                  Gaps.vGap5,
                  _buildRatingHorizontalView(),
                   Gaps.vGap5,
              
                ])));
  }

  Widget _textKamStatus = new Container(
    margin: const EdgeInsets.fromLTRB(5, 0, 10, 0),
    padding: const EdgeInsets.fromLTRB(6, 3, 6, 3),
    decoration: BoxDecoration(
        color: ColorsHelper.WHITE,
        borderRadius: BorderRadius.all(Radius.circular(25)),
        border: Border.all(width: 1, style: BorderStyle.none)),
    child: Text("KAM",
        style: TextStyle(color: ColorsHelper.PRIMARY_COLOR, fontSize: 10)),
  );

  Widget _circularShape = new Container(
    margin: const EdgeInsets.fromLTRB(10, 3, 0, 5),
    padding: const EdgeInsets.all(3),
    decoration: BoxDecoration(
        color: ColorsHelper.WHITE,
        borderRadius: BorderRadius.all(Radius.circular(40)),
        border: Border.all(width: 1, style: BorderStyle.none)),
    child: Icon(
      Icons.camera_enhance,
      size: 12,
      color: ColorsHelper.PRIMARY_COLOR,
    ),
  );

  Widget _buildDetails() {
    return SliverList(
        delegate: SliverChildListDelegate(<Widget>[
      // Gaps.vGap12,
      // Container(
      //     margin: EdgeInsets.fromLTRB(35, 0, 35, 5),
      //     child: MaterialSegmentedControl(
      //       children: _children(),
      //       selectionIndex: _currentSelection,
      //       borderColor: ColorsHelper.PRIMARY_COLOR,
      //       selectedColor: ColorsHelper.PRIMARY_COLOR,
      //       unselectedColor: Colors.white,
      //       borderRadius: 32.0,
      //       horizontalPadding: EdgeInsets.all(0),
      //       onSegmentChosen: (index) {
      //         setState(() {
      //           _currentSelection = index;
      //         });
      //       },
      //     )),

      ListView.separated(
          // physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          primary: false,
          padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
          itemCount: vReview.length,
          itemBuilder: (_, int position) => ReviewItemTask(
                review: vReview[position],
                // onPressed: () => pushNewPage(
                //     context,
                //     BookDetailsPage(
                //         id: vTask[position].id,
                //         imageUrl: vTask[position].cover))
              ),
          separatorBuilder: (BuildContext context, int index) => Gaps.vGap2),
      Gaps.vGap10,
      Container(
          padding: new EdgeInsets.fromLTRB(7.0, 0.0, 5.0, 0.0),
          margin: EdgeInsets.fromLTRB(7.0, 0.0, 5.0, 0.0),
          child: Text(
            "BADGES",
            style: TextStyle(
              fontSize: 12,
              color: ColorsHelper.TRUX_GREY_4_COLOR,
            ),
          )),
      Gaps.vGap3,
      ListView.separated(
          // physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          primary: false,
          padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
          itemCount: vReview.length,
          itemBuilder: (_, int position) => BadgeItemTask(
                review: vReview[position],
                // onPressed: () => pushNewPage(
                //     context,
                //     BookDetailsPage(
                //         id: vTask[position].id,
                //         imageUrl: vTask[position].cover))
              ),
          separatorBuilder: (BuildContext context, int index) => Gaps.vGap1),
    ]));
  }

  Map<int, Widget> _children() =>
      {0: Text('As a Tasker'), 1: Text('As a Poster')};

  ///  Horizontal Rating line
  Widget _buildRatingHorizontalView() {
    return Container(
        width: double.infinity,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            SmoothStarRating(
                rating: 3.5,
                size: 18,
                allowHalfRating: false,
                color: ColorsHelper.PRIMARY_COLOR),
            Container(
                padding: new EdgeInsets.fromLTRB(5.0, 0.0, 8.0, 0.0),
                child: Expanded(child: Text('1 Reviews'))),
          ]),
          Gaps.vGap6,
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
                padding: new EdgeInsets.fromLTRB(5.0, 0.0, 3.0, 0.0),
                child: Expanded(
                  child: Text('33% Completions rate'),
                )),
            Container(
                padding: new EdgeInsets.fromLTRB(1.0, 0.0, 2.0, 0.0),
                child: Icon(
                  Icons.info,
                  size: 14,
                  color: ColorsHelper.COLOR_GREY_300,
                )),
          ])
        ]));
  }
}
