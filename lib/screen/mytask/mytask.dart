import 'package:flutter/material.dart';
import 'package:trux24/model/task.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/screen/taskdetails/taskdetails.dart';
import 'package:trux24/ui_widget/tasklist/list_item_task.dart';

class MyTaskScreen extends StatefulWidget {
  @override
  _MyTaskUI createState() => _MyTaskUI();
}

class _MyTaskUI extends State<MyTaskScreen> {
  List<Task> vTask = new List();

  @override
  void initState() {
    super.initState();
    vTask.add(new Task(0, 100));
    vTask.add(new Task(1, 101));
    vTask.add(new Task(2, 102));
    vTask.add(new Task(3, 103));
    vTask.add(new Task(4, 104));
    print(vTask);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorsHelper.TRUX_GREY_2_COLOR,
        body: Container(
  
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/bg_image/all_bg.png"),
              fit: BoxFit.cover,
            )),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                ListView.separated(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    padding:  EdgeInsets.fromLTRB(4, 0, 4, 0),
                    itemCount: vTask.length,
                    itemBuilder: (_, int position) => ItemTask(
                          task: vTask[position],
                           onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => TaskDetailsScreen()));
        }
                          // onPressed: () => pushNewPage(
                          //     context,
                          //     BookDetailsPage(
                          //         id: vTask[position].id,
                          //         imageUrl: vTask[position].cover))
                        ),
                    separatorBuilder: (BuildContext context, int index) =>
                        Gaps.vGap3)
              ]),
            )));
  }
}
