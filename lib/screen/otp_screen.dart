import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/screen/main/main_bottom_menu.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trux24/widgets/pin_input/pin_put.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpUI createState() => _OtpUI();
}

class _OtpUI extends State<OtpScreen> {
  final String assetName = "assets/bg_image/head_otp_screen.svg";
  final String mobileImg = "assets/svg_image/mob-icon02.svg";
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  BuildContext _context;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorsHelper.PRIMARY_COLOR,
//            leading: IconButton(
//                icon: Icon(
//                  Icons.list,
//                  color: Colors.white,
//                ),
//                onPressed: () {}),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/bg_image/all_bg.png"),
            fit: BoxFit.cover,
          )),

          /// Set component layout
          child: Column( 
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
            new Expanded(
              flex: 3,
              child: Stack(children: <Widget>[
                new Container(
                    width: MediaQuery.of(context).size.width,
                    color: ColorsHelper.COLOR_BLUE_50,
                    child: SvgPicture.asset(
                      assetName,
                      color: ColorsHelper.PRIMARY_COLOR,
                      fit: BoxFit.fill,
                    )),
                new Column(children: <Widget>[
                  new Expanded(
                    flex: 2,
                    child: new Container(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 100,
                        height: 110,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image:
                                AssetImage("assets/bg_image/mobile_icon.png"),
                          ),
                        ),
                      ),
                    ),
                  ),
                  new Expanded(
                    flex: 3,
                    child: new Container(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                UIStringHelper.verificationCodeLabel,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: ColorsHelper
                                        .TRUX_GREY_DARK_LIGHT_COLOR),
                              ),
                              Gaps.vGap4,
                              Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Container(
                                      margin: EdgeInsets.only(bottom: 2),
                                      alignment:
                                          AlignmentDirectional.bottomCenter,
                                      child: Text(
                                        UIStringHelper.pleaseGiveYourOtpNumber,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: ColorsHelper.WHITE),
                                      ))),
                                Gaps.vGap4, //variable above
                            ])),
                  ),
                    
                ])
              ]),

              // child: Stack(children: <Widget>[
              //   Column(mainAxisSize: MainAxisSize.max, children: <Widget>[

              //   ]),
              // ])
            ),
            new Expanded(
                flex: 4,
                child: Stack(children: <Widget>[
                  Column(children: <Widget>[
                    Gaps.vGap4,
                    Container(
                           margin: EdgeInsets.fromLTRB(10,5,10,5),
                           child: animatingOTPView(),
                    ),
                    Gaps.vGap3,
                    _buildResendView(),
                     Gaps.vGap4,
                    _buildContinueButtonView(),
                    Gaps.vGap5,
                    // Padding(
                    //   padding: EdgeInsets.all(5.0),
                    //   child: Container(
                    //       margin: EdgeInsets.only(bottom: 10),
                    //       alignment: AlignmentDirectional.bottomCenter,
                    //       height: 60,
                    //       child: Text('Version 1.0.6')),
                    // )
                  ])
                ]))
          ])),
    );
  }

  Widget _buildContinueButtonView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: ButtonWidget(
          color: ColorsHelper.PRIMARY_COLOR,
          shadow: UIHelper.MUZ_BUTTONSHADOW,
          text: UIStringHelper.btnContinue,
          topPadding: 7,
          circleAvatar: true,
          onPressed: () {
            // Navigator.pop(context);
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => MainMenuScreen()));

            print("Tapped Hello");
          },
        ),
      ),
    );
  }

  Widget animatingOTPView() {
    BoxDecoration pinPutDecoration = BoxDecoration(
      color: ColorsHelper.WHITE,
      border: Border.all(color: ColorsHelper.COLOR_GREY_400),
      borderRadius: BorderRadius.circular(5),
    );
    return PinPut(
      fieldsCount: 4,
      eachFieldWidth: 52,
      eachFieldHeight: 54,
      textStyle: TextStyle(fontSize: 25, color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR),
      eachFieldMargin : EdgeInsets.only(left:5),
      fieldsAlignment: MainAxisAlignment.center,
      onSubmit: (String pin) => _showSnackBar(pin),
      focusNode: _pinPutFocusNode,
      controller: _pinPutController,
      submittedFieldDecoration : pinPutDecoration,
      selectedFieldDecoration: pinPutDecoration,
      pinAnimationType: PinAnimationType.slide,
      followingFieldDecoration: pinPutDecoration
    );
  }

 Widget _buildResendView() {
    return Padding(
                    padding: EdgeInsets.all(0.0),
                    child: Container(
                        child: Text('RESEND',
                         style: new TextStyle(
                           fontWeight: FontWeight.bold,
                         color: ColorsHelper.PRIMARY_COLOR, fontSize: 12),
                      textAlign: TextAlign.center,)),
                  );
 }
  void _showSnackBar(String pin) {
    final snackBar = SnackBar(
      duration: Duration(seconds: 3),
      content: Container(
          height: 80.0,
          child: Center(
            child: Text(
              'Pin Submitted. Value: $pin',
              style: TextStyle(fontSize: 25.0),
            ),
          )),
      backgroundColor: Colors.deepPurpleAccent,
    );
    Scaffold.of(_context).hideCurrentSnackBar();
    Scaffold.of(_context).showSnackBar(snackBar);
  }
}
