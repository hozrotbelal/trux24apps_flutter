import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:trux24/screen/login_with_otp.dart';

class IntroductionScreen extends StatefulWidget {
  @override
  _IntroductionUI createState() => _IntroductionUI();
}

class _IntroductionUI extends State<IntroductionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorsHelper.PRIMARY_COLOR,
        title: Text('WelCome',
            style: TextStyle(fontSize: 20, color: ColorsHelper.WHITE)),
//            leading: IconButton(
//                icon: Icon(
//                  Icons.list,
//                  color: Colors.white,
//                ),
//                onPressed: () {}),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/bg_image/all_bg.png"),
            fit: BoxFit.cover,
          )),

          /// Set component layout
          child: Column(children: <Widget>[
            new Expanded(
                flex: 9,
                child: Stack(children: <Widget>[
                  Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                    new SingleChildScrollView(
                        child: Container(
                            alignment: AlignmentDirectional.topCenter,
                            child: Column(children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 300,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage(
                                        "assets/bg_image/intro_banner.png"),
                                  ),
                                ),
                              ),
                              SizedBox(height: 10),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text(
                                    UIStringHelper
                                        .introduction_screen_catch_phrase_title_1,
                                    textAlign: TextAlign.center,
                                    style: new TextStyle(
                                        fontSize: 19.0,
                                        color:
                                            ColorsHelper.TRUX_GREY_DARK_COLOR),
                                  ),
                                ),
                              ),
                              SizedBox(height: 2),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Padding(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text(
                                    UIStringHelper
                                        .introduction_screen_catch_phrase_text,
                                    textAlign: TextAlign.center,
                                    style: new TextStyle(
                                        fontSize: 16.0,
                                        color: ColorsHelper
                                            .TRUX_GREY_DARK_LIGHT_COLOR),
                                  ),
                                ),
                              ),
                              SizedBox(height: 2),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Padding(
                                  padding: EdgeInsets.all(5.0),
                                  child: ButtonWidget(
                                    color: ColorsHelper.PRIMARY_COLOR,
                                    shadow: UIHelper.MUZ_BUTTONSHADOW,
                                    text: UIStringHelper.introStart,
                                    topPadding: 7,
                                    circleAvatar: true,
                                    onPressed: () {
                                      // Navigator.pop(context);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LoginWithOtpScreen()));
                                      print("Tapped Hello");
                                    },
                                  ),
                                ),
                              ),
                            ]))),
                  ]),
                ])),
            new Expanded(
                flex: 1,
                child: Stack(children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: AlignmentDirectional.bottomCenter,
                        height: 60,
                        child: Text('Version 1.0.0')),
                  )
                ]))
          ])),
    );
  }
}
