import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/section/section_richtext_view.dart';
import 'package:trux24/ui_widget/section/section_view.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:trux24/widgets/line.dart';
import 'package:dotted_line/dotted_line.dart';

class TaskPostDueDateScreen extends StatefulWidget {
  @override
  _TaskPostDueDateUI createState() => _TaskPostDueDateUI();
}

class _TaskPostDueDateUI extends State<TaskPostDueDateScreen> {
  TextEditingController _datePickerController = TextEditingController();
  TextEditingController _timePickerController = TextEditingController();
  TextEditingController _priceBidDurationController = TextEditingController();
  TextEditingController _labourCountController = TextEditingController();

  FocusNode _datePickerBudget = FocusNode();
  FocusNode _priceBidDuration = FocusNode();
  FocusNode _labourCount = FocusNode();

  final GlobalKey _keyDashPickUp = GlobalKey();
  final GlobalKey _keyDashDropUp = GlobalKey();

  Size dashSizePickUp, dashSizeDropUp;
  Offset dashPositionPickUp, dashPositionDropUp;
  double dashHeightPickUp = 0.0, dashHeightDropUp = 0.0;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _datePickerController?.dispose();
    _datePickerBudget?.dispose();
    _priceBidDurationController?.dispose();
    _priceBidDuration?.dispose();
    _labourCountController?.dispose();
    _labourCount?.dispose();
  }

  getSizeAndPosition() {
    RenderBox renderBox = _keyDashPickUp.currentContext.findRenderObject();
    dashSizePickUp = renderBox.size;
    dashHeightPickUp = dashSizePickUp.height;
    dashPositionPickUp = renderBox.localToGlobal(Offset.zero);

    RenderBox renderBoxDropUo =
        _keyDashPickUp.currentContext.findRenderObject();
    dashSizeDropUp = renderBoxDropUo.size;
    dashHeightDropUp = dashSizeDropUp.height;
    print(dashSizePickUp);
    print("dashHeight of Dash: $dashHeightPickUp ");
    print("Size of Dash Drop Up: $dashSizeDropUp ");
    print(dashPositionPickUp);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10, top: 2, bottom: 2),
      child: Column(
        // center the children
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Gaps.vGap8,
          SectionRichTextView(
            UIStringHelper.postTaskDueDateLocationLabel,
            onPressed: () {},
            more: " *",
            textColor: ColorsHelper.TRUX_GREY_4_COLOR,
            leftTextSize: 15,
            padding: EdgeInsets.all(1),
          ),
          _buildTruckCountView(),
          Gaps.vGap10,
          SectionRichTextView(
            UIStringHelper.postTaskDueDateScheduleLabel,
            onPressed: () {},
            more: "",
            leftTextSize: 15,
            textColor: ColorsHelper.TRUX_GREY_4_COLOR,
            padding: EdgeInsets.all(1),
          ),
          Container(
            //padding: EdgeInsets.all(3),
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: 42,
            child: CustomTextField(
              focusNode: _datePickerBudget,
              controller: _datePickerController,
              etEnabled: false,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 0, top: 2),
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                prefixIcon: Icon(
                  Icons.date_range,
                  size: 25,
                  color: ColorsHelper.COLOR_GREY_300,
                ),
                suffixIcon: Icon(
                  Icons.arrow_drop_down,
                  size: 25,
                  color: ColorsHelper.COLOR_GREY_600,
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
          ),
          Container(
            //padding: EdgeInsets.all(3),
            margin: const EdgeInsets.fromLTRB(0, 7, 0, 0),
            height: 42,
            child: CustomTextField(
              focusNode: _datePickerBudget,
              controller: _datePickerController,
              etEnabled: false,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 0, top: 2),
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                prefixIcon: Icon(
                  Icons.timer,
                  size: 25,
                  color: ColorsHelper.COLOR_GREY_300,
                ),
                suffixIcon: Icon(
                  Icons.arrow_drop_down,
                  size: 25,
                  color: ColorsHelper.COLOR_GREY_600,
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
          ),
          Gaps.vGap20,
          SectionRichTextView(
            UIStringHelper.postTaskDueDatePriceBidDurationLabel,
            onPressed: () {},
            more: " ",
            leftTextSize: 15,
            textColor: ColorsHelper.TRUX_GREY_4_COLOR,
            padding: EdgeInsets.all(1),
          ),
          Container(
            //padding: EdgeInsets.all(3),
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: 42,
            child: CustomTextField(
              focusNode: _priceBidDuration,
              controller: _priceBidDurationController,
              etEnabled: true,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 10, top: 3),
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
          ),
          Gaps.vGap40,
        ],
      ),
    );
  }

  /// Or Horizontal View
  Widget _buildTruckCountView() {
    return Container(
        padding: const EdgeInsets.only(top: 15, left: 5, right: 5, bottom: 20),
        margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        color: ColorsHelper.WHITE,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.fromLTRB(5, 0, 1, 0),
                      width: 30,
                      child: Stack(alignment: Alignment.topLeft, children: <
                          Widget>[
                        Container(
                            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            height: 23,
                            width: 23,
                            padding: const EdgeInsets.all(1),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color:
                                        ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                                    width: 2,
                                    style: BorderStyle.solid),
                                shape: BoxShape.circle,
                                color: Colors.white),
                            child: Icon(
                              Icons.place,
                              size: 15,
                              color: ColorsHelper.COLOR_GREY_600,
                            )),
                        Container(
                            margin: const EdgeInsets.fromLTRB(11.5, 26, 0, 0),
                            child: DottedLine(
                              dashLength: 4,
                              lineThickness: 2,
                              dashColor: ColorsHelper.COLOR_GREY_500,
                              dashGapColor: Colors.white,
                              direction: Axis.vertical,
                              lineLength: dashHeightPickUp,
                            )),
                      ]),
                      // child: Container(
                      //     padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                      //     child: Text("Apple and Android Application Good Job",
                      //         style: TextStyle(
                      //           fontSize: 14,
                      //           color: ColorsHelper.TRUX_GREY_4_COLOR,
                      //         )))
                    ),
                    Expanded(
                        key: _keyDashPickUp,
                        child: Container(
                            //padding: EdgeInsets.all(3),
                            margin: const EdgeInsets.only(
                                left: 1, right: 5, top: 0, bottom: 0),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      UIStringHelper
                                          .postTaskDueDatePickUpLocationLabel,
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontStyle: FontStyle.normal,
                                        // fontWeight: FontWeight.w400,
                                        color: ColorsHelper.TRUX_GREY_5_COLOR,
                                      )),
                                  Container(
                                      //padding: EdgeInsets.all(3),
                                      margin: const EdgeInsets.only(
                                          left: 0, top: 5),
                                      child: Text(
                                        UIStringHelper
                                            .postTaskDueDateSelectPickUpLocationLabel,
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontStyle: FontStyle.normal,
                                          // fontWeight: FontWeight.w400,
                                          color: ColorsHelper.TRUX_GREY_3_COLOR,
                                        ),
                                      )),
                                ]))),
                    Container(
                        padding: new EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: _circularShape),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.fromLTRB(5, 0, 1, 0),
                      width: 30,
                      child: Stack(alignment: Alignment.topLeft, children: <
                          Widget>[
                        Container(
                            margin: const EdgeInsets.fromLTRB(11.5, 2, 0, 0),
                            child: DottedLine(
                              dashLength: 0,
                              lineThickness: 2,
                              dashColor: ColorsHelper.PRIMARY_COLOR,
                              dashGapColor: ColorsHelper.PRIMARY_COLOR,
                              direction: Axis.vertical,
                              lineLength: 10,
                            )),
                        Container(
                            margin: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                            height: 23,
                            width: 23,
                            padding: const EdgeInsets.all(1),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: ColorsHelper.PRIMARY_COLOR,
                                    width: 2,
                                    style: BorderStyle.solid),
                                shape: BoxShape.circle,
                                color: Colors.white),
                            child: Icon(
                              Icons.place,
                              size: 15,
                              color: ColorsHelper.PRIMARY_COLOR,
                            )),
                        Container(
                            //padding: EdgeInsets.all(3),
                            margin: const EdgeInsets.fromLTRB(0, 35, 0, 0),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      margin: const EdgeInsets.fromLTRB(
                                          11.5, 0, 0, 0),
                                      child: DottedLine(
                                        dashLength: 4,
                                        lineThickness: 2,
                                        dashColor: ColorsHelper.PRIMARY_COLOR,
                                        dashGapColor: ColorsHelper.WHITE,
                                        direction: Axis.vertical,
                                        lineLength: dashHeightDropUp,
                                      )),
                                  Container(
                                      margin: const EdgeInsets.fromLTRB(
                                          11.5, 0, 0, 0),
                                      child: DottedLine(
                                        lineThickness: 2,
                                        dashColor: ColorsHelper.PRIMARY_COLOR,
                                        dashGapColor:
                                            ColorsHelper.PRIMARY_COLOR,
                                        direction: Axis.vertical,
                                        lineLength: 10,
                                      )),
                                  Container(
                                      margin: const EdgeInsets.fromLTRB(
                                          6.5, 0, 0, 0),
                                      child: DottedLine(
                                        dashLength: 0,
                                        lineThickness: 2,
                                        dashColor: ColorsHelper.PRIMARY_COLOR,
                                        dashGapColor:
                                            ColorsHelper.PRIMARY_COLOR,
                                        direction: Axis.horizontal,
                                        lineLength: 14,
                                      )),
                                ])),
                      ]),
                    ),
                    Expanded(
                        key: _keyDashDropUp,
                        child: Container(
                            //padding: EdgeInsets.all(3),
                            margin: const EdgeInsets.only(
                                left: 1, right: 5, top: 10, bottom: 0),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      UIStringHelper
                                          .postTaskDueDateDropOffLocationLabel,
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontStyle: FontStyle.normal,
                                        color: ColorsHelper.TRUX_GREY_5_COLOR,
                                      )),
                                  Container(
                                      //padding: EdgeInsets.all(3),
                                      margin: const EdgeInsets.only(
                                          left: 0, top: 6),
                                      child: Text(
                                        UIStringHelper
                                            .postTaskDueDateSelectDropOffLocationLabel,
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontStyle: FontStyle.normal,
                                          color: ColorsHelper.TRUX_GREY_3_COLOR,
                                        ),
                                      )),
                                ]))),
                    Container(
                        padding: new EdgeInsets.fromLTRB(0, 0, 0, 0),
                        margin: const EdgeInsets.only(
                            left: 0, right: 0, top: 7, bottom: 0),
                        child: _circularShape),
                  ]),
            ]));
  }

  Widget _circularShape = new Container(
    margin: const EdgeInsets.only(left: 3, right: 12, top: 0, bottom: 0),
    height: 25,
    width: 25,
    padding: const EdgeInsets.all(1),
    decoration: BoxDecoration(
        border: Border.all(
            color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
            width: 1,
            style: BorderStyle.solid),
        shape: BoxShape.circle,
        color: Colors.white),
    child: Icon(
      Icons.chevron_right,
      color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
      size: 18,
    ),
  );

  _getSizes() {
    final RenderBox renderBoxRed =
        _keyDashPickUp.currentContext.findRenderObject();
    final sizeRed = renderBoxRed.size;
    print("SIZE of Dash: $sizeRed");
  }

  _getPositions() {
    final RenderBox renderBoxRed =
        _keyDashPickUp.currentContext.findRenderObject();
    final positionRed = renderBoxRed.localToGlobal(Offset.zero);
    print("POSITION of Dash: $positionRed ");
  }
}
