import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/section/section_view.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:dotted_border/dotted_border.dart';

class TaskPostBudgetScreen extends StatefulWidget {
  @override
  _TaskPostBudgetUI createState() => _TaskPostBudgetUI();
}

class _TaskPostBudgetUI extends State<TaskPostBudgetScreen> {
  TextEditingController _estimatedBudgetController = TextEditingController();
  TextEditingController _truckCountController = TextEditingController();
  TextEditingController _labourCountController = TextEditingController();

  FocusNode _estimatedBudget = FocusNode();
  FocusNode _truckCount = FocusNode();
  FocusNode _labourCount = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _estimatedBudgetController?.dispose();
    _estimatedBudget?.dispose();
    _truckCountController?.dispose();
    _truckCount?.dispose();
    _labourCountController?.dispose();
    _labourCount?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return   SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child:  Container(
      margin: const EdgeInsets.only(left: 10, right: 10, top: 2, bottom: 2),
      child: Column(
        // center the children
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Gaps.vGap8,
          SectionView(UIStringHelper.postTaskBudgetLabel,
              onPressed: () {},
              hiddenMore: true,
              headerBgTopColor: ColorsHelper.TRUX_GREY_3_COLOR,
              textLeftHeaderColor: ColorsHelper.PRIMARY_COLOR,
          

              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      //padding: EdgeInsets.all(3),
                      margin: const EdgeInsets.only(
                          left: 7, right: 7, top: 10, bottom: 10),
                      height: 45,
                      child: CustomTextField(
                        focusNode: _estimatedBudget,
                        controller: _estimatedBudgetController,
                        etEnabled: true,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 3, top: 3),
                          counterText: "",
                          filled: true,
                          fillColor: ColorsHelper.WHITE,
                          prefixIcon: ImageIcon(
                            AssetImage("assets/images/ic_taka_12.png"),
                            size: 12,
                            color: ColorsHelper.BLACK,
                          ),
                          // suffixIcon: _circularShape,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red, //this has no effect
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                            borderSide: BorderSide(
                                color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                            borderSide: BorderSide(
                                color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                          ),
                        ),
                      ),
                    ),
                    Gaps.vGap20,
                    Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                        child: Text(UIStringHelper.postTaskBudgetEstimatedLabel,
                            style: TextStyle(
                              fontSize: 16,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w700,
                              color: ColorsHelper.TRUX_GREY_4_COLOR,
                            ))),
                    Gaps.vGap20,
                    _buildTruckCountView(),
                    Gaps.vGap20,
                    _buildLabourCountView(),
                    Gaps.vGap20,
                  ])),
          Gaps.vGap30,
          SectionView(
            UIStringHelper.postTaskBudgetUploadImagesLabel,
            onPressed: () {},
            hiddenMore: true,
            headerBgTopColor: ColorsHelper.TRUX_GREY_3_COLOR,
            textLeftHeaderColor: ColorsHelper.PRIMARY_COLOR,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.only( left: 12, right: 12, top: 40, bottom: 40),
                  height: 100,
                  width: 100,
            
                child: DottedBorder(
                  color: ColorsHelper.TRUX_GREY_4_COLOR,
                  dashPattern: [4, 4],
                  strokeWidth: 1,
                  strokeCap: StrokeCap.round,
                  radius: Radius.circular(12),
                  child:Align(
                  alignment: Alignment.center,
                  child: Container(
                        margin: const EdgeInsets.only(left: 7, right: 7, top: 20, bottom: 20),
                        height: 55,
                        width: 60,
                        decoration: BoxDecoration(
                        image: DecorationImage(
                        image: AssetImage("assets/images/plus_icon.png"),
                  
          ))),
                  ),
                ),
              ),
            ),
          ),
          Gaps.vGap30,
        ],
      ),
    ));
  }

  /// Or Horizontal View
  Widget _buildTruckCountView() {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0),
      child: Center(
          child: Row(children: <Widget>[
        Expanded(
            child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                child: Text(UIStringHelper.postTaskBudgetTruckTaskersLabel,
                    style: TextStyle(
                      fontSize: 14,
                      color: ColorsHelper.TRUX_GREY_4_COLOR,
                    )))),
        Container(
            //padding: EdgeInsets.all(3),
            margin:
                const EdgeInsets.only(left: 3, right: 10, top: 0, bottom: 0),
            width: 60,
            height: 30,
            child: CustomTextField(
              focusNode: _truckCount,
              controller: _truckCountController,
              etEnabled: true,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              isShowIconDelete: false,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 4, right: 4),
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                // suffixIcon: _circularShape,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            )),
        Container(
          height: 32,
          width: 37,
          decoration: BoxDecoration(
              color: ColorsHelper.PRIMARY_COLOR,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  bottomLeft: Radius.circular(40)),
              border: Border.all(
                  width: 3,
                  color: ColorsHelper.PRIMARY_COLOR,
                  style: BorderStyle.solid)),
          child: Center(
              child: Text(
            "-",
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: ColorsHelper.WHITE,
                height: 1.5),
          )),
        ),
        Container(
            padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            width: 1.0,
            height: 32.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                ColorsHelper.TRUX_GREY_2_COLOR,
                ColorsHelper.TRUX_GREY_2_COLOR
              ]),
            )),
        Container(
          height: 32,
          width: 37,
          margin: const EdgeInsets.only(left: 0, right: 10, top: 0, bottom: 0),
          decoration: BoxDecoration(
              color: ColorsHelper.PRIMARY_COLOR,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
              border: Border.all(
                  width: 3,
                  color: ColorsHelper.PRIMARY_COLOR,
                  style: BorderStyle.solid)),
          child: Center(
              child: Text(
            "+",
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: ColorsHelper.WHITE,
                height: 1.5),
          )),
        ),
      ])),
    );
  }

  /// Or Horizontal View
  Widget _buildLabourCountView() {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0),
      child: Center(
          child: Row(children: <Widget>[
        Expanded(
            child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                child: Text(UIStringHelper.postTaskBudgetLaborTaskersLabel,
                    style: TextStyle(
                      fontSize: 14,
                      color: ColorsHelper.TRUX_GREY_4_COLOR,
                    )))),
        Container(
            //padding: EdgeInsets.all(3),
            margin:
                const EdgeInsets.only(left: 3, right: 10, top: 0, bottom: 0),
            width: 60,
            height: 30,
            child: CustomTextField(
              focusNode: _labourCount,
              controller: _labourCountController,
              etEnabled: true,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
                isShowIconDelete: false,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 4, right: 4),
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                // suffixIcon: _circularShape,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            )),
        Container(
          height: 32,
          width: 37,
          decoration: BoxDecoration(
              color: ColorsHelper.PRIMARY_COLOR,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  bottomLeft: Radius.circular(40)),
              border: Border.all(
                  width: 3,
                  color: ColorsHelper.PRIMARY_COLOR,
                  style: BorderStyle.solid)),
          child: Center(
              child: Text(
            "-",
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: ColorsHelper.WHITE,
                height: 1.5),
          )),
        ),
        Container(
            padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            width: 1.0,
            height: 32.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                ColorsHelper.TRUX_GREY_2_COLOR,
                ColorsHelper.TRUX_GREY_2_COLOR
              ]),
            )),
        Container(
          height: 32,
          width: 37,
          margin: const EdgeInsets.only(left: 0, right: 10, top: 0, bottom: 0),
          decoration: BoxDecoration(
              color: ColorsHelper.PRIMARY_COLOR,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
              border: Border.all(
                  width: 3,
                  color: ColorsHelper.PRIMARY_COLOR,
                  style: BorderStyle.solid)),
          child: Center(
              child: Text(
            "+",
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                color: ColorsHelper.WHITE,
                height: 1.5),
          )),
        ),
      ])),
    );
  }
}
