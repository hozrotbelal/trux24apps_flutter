import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/model/taskpost/vehicle_type.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/taskpost/list_vehicle_type.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:trux24/widgets/switch/custom_switch.dart';


class First extends StatefulWidget {
  @override
  _FirstUI createState() => _FirstUI();
}

class _FirstUI extends State<First> {
  List<VehicleType> mVehicleTypeList = new List();
  List<VehicleType> mVehicleCapacityList = new List();
  List<VehicleType> mVehicleSizeList = new List();
  bool values = false;
  String txtYesOrNo = UIStringHelper.postTaskDetailsTaskareYouSureLabelNO;
  TextEditingController _goodsTypeController = TextEditingController();
  TextEditingController _describeController = TextEditingController();

  final FocusNode _goodsTypePhone = FocusNode();
  final FocusNode _nodeDescribe = FocusNode();


    
  
  @override
  Widget build(BuildContext context) {
    mVehicleTypeList.add(new VehicleType(0, "All", ""));
    mVehicleTypeList.add(new VehicleType(1, "Open Truck", ""));
    mVehicleTypeList.add(new VehicleType(2, "Covered Van", ""));

    mVehicleCapacityList.add(new VehicleType(0, "All", ""));
    mVehicleCapacityList.add(new VehicleType(1, "1 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(2, "2 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(4, "3.5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(5, "5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(6, "7.5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(7, "10 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(8, "15 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(9, "25 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(10, "25+ Ton", ""));

    mVehicleSizeList.add(new VehicleType(0, "7 Feet", ""));
    mVehicleSizeList.add(new VehicleType(1, "10 Feet", ""));
    mVehicleSizeList.add(new VehicleType(2, "12 Feet", ""));
    mVehicleSizeList.add(new VehicleType(4, "16 Feet", ""));
    mVehicleSizeList.add(new VehicleType(5, "18 Feet", ""));
    mVehicleSizeList.add(new VehicleType(6, "22 Feet", ""));
    mVehicleSizeList.add(new VehicleType(7, "22+ Feet", ""));
    print(mVehicleTypeList);
    return Container(
        child: Center(
        child: Column(
          // center the children
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
     
        buildFSwitchWithChild(),
  //           Gaps.vGap10,
  //           RichText(
  //             // overflow: TextOverflow.ellipsis,
  //             textAlign: TextAlign.start,
  //             text: TextSpan(
  //               children: [
  //                 TextSpan(
  //                     text: UIStringHelper.postTaskDetailsTruckTypeLabel,
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.TRUX_GREY_4_COLOR,
  //                         fontSize: 13)),
  //                 TextSpan(
  //                     text: " *",
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.COLOR_RED_DOT_DARK,
  //                         fontSize: 13)),
  //               ],
  //             ),
  //           ),
  //           Container(
  //             margin: const EdgeInsets.only(top: 5, bottom: 7.5),
  //             padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
  //             height: 55,
  //             color: ColorsHelper.WHITE,
  //             child: ItemVehicleSelector(
  //               mVehicleTypeList: mVehicleTypeList,
  //             ),
  //           ),
  //           Gaps.vGap5,
  //           RichText(
  //             // overflow: TextOverflow.ellipsis,
  //             textAlign: TextAlign.start,
  //             text: TextSpan(
  //               children: [
  //                 TextSpan(
  //                     text: UIStringHelper.postTaskDetailsTruckTypeLabel,
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.TRUX_GREY_4_COLOR,
  //                         fontSize: 13)),
  //                 TextSpan(
  //                     text: " *",
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.COLOR_RED_DOT_DARK,
  //                         fontSize: 13)),
  //               ],
  //             ),
  //           ),
  //           Container(
  //             margin: const EdgeInsets.only(top: 5, bottom: 7.5),
  //             padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
  //             height: 55,
  //             color: ColorsHelper.WHITE,
  //             child: ItemVehicleSelector(
  //               mVehicleTypeList: mVehicleCapacityList,
  //             ),
  //           ),
  //           Gaps.vGap5,
  //           RichText(
  //             // overflow: TextOverflow.ellipsis,
  //             textAlign: TextAlign.start,
  //             text: TextSpan(
  //               children: [
  //                 TextSpan(
  //                     text: UIStringHelper.postTaskDetailsTruckSizeLabel,
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.TRUX_GREY_4_COLOR,
  //                         fontSize: 13)),
  //                 TextSpan(
  //                     text: " *",
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.COLOR_RED_DOT_DARK,
  //                         fontSize: 13)),
  //               ],
  //             ),
  //           ),
  //           Container(
  //             margin: const EdgeInsets.only(top: 5, bottom: 7.5),
  //             padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
  //             height: 55,
  //             color: ColorsHelper.WHITE,
  //             child: ItemVehicleSelector(
  //               mVehicleTypeList: mVehicleSizeList,
  //             ),
  //           ),
  // Gaps.vGap5,
  //           RichText(
  //             // overflow: TextOverflow.ellipsis,
  //             textAlign: TextAlign.start,
  //             text: TextSpan(
  //               children: [
  //                 TextSpan(
  //                     text: UIStringHelper.postTaskDetailsSelectGoodsTypeLabel,
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.w600,
  //                         color: ColorsHelper.TRUX_GREY_4_COLOR,
  //                         fontSize: 13)),
                
  //               ],
  //             ),
  //           ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),
            // Icon(
            //   Icons.favorite,
            //   size: 160.0,
            //   color: Colors.red,
            // ),

      
            Text("First Tab"),
            
            Gaps.vGap5,
             Container(
              margin: const EdgeInsets.only(top: 5, bottom: 7.5),
              padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
              height: 55,
              color: ColorsHelper.WHITE,
              child: ItemVehicleSelector(
                mVehicleTypeList: mVehicleCapacityList,
              ),
            ),
            Gaps.vGap5,
             Container(
              margin: const EdgeInsets.only(top: 5, bottom: 7.5),
              padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
              height: 55,
              color: ColorsHelper.WHITE,
              child: ItemVehicleSelector(
                mVehicleTypeList: mVehicleCapacityList,
              ),
            ),
            Gaps.vGap5,
             Container(
              //padding: EdgeInsets.all(3),
              height: 45,
              child: CustomTextField(
              focusNode: _goodsTypePhone,
              controller: _goodsTypeController,
            
              etEnabled: false,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: UIStringHelper.postTaskDetailsSelectYourGoodsHint,
                
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                // suffixIcon : _circularShape,

                border: OutlineInputBorder(
                 
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(0.0),
                
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide: BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide: BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
             ),
              Gaps.vGap5,
            RichText(
              // overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.start,
              text: TextSpan(
                children: [
                  TextSpan(
                      text: UIStringHelper.postTaskDetailsDescripbeTripLabel,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: ColorsHelper.TRUX_GREY_4_COLOR,
                          fontSize: 13)),
                  TextSpan(
                      text: " *",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: ColorsHelper.COLOR_RED_DOT_DARK,
                          fontSize: 13)),
                ],
              ),
            ),
            Gaps.vGap8,
            CustomTextField(
              
              focusNode: _nodeDescribe,
              controller: _describeController,
              minLines: 5,
              maxLines: 15,
              etEnabled: true,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: UIStringHelper.postTaskDetailsDescriptionHint,
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
              
                border: OutlineInputBorder(
                 
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(0.0),
                
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide: BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide: BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
            SizedBox(
              height: 300
            ),
          ],
        )),
    
     );

  
  }

    Container buildFSwitchWithChild() {
    return Container(
        // margin: const EdgeInsets.only(top: 5, bottom: 7.5),
        padding: EdgeInsets.all(4),
        color: ColorsHelper.WHITE,
        child: Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text('$txtYesOrNo',
                          style: TextStyle(
                              fontSize: 12,
                              color: ColorsHelper.AIR_RED_COLOR))),
                  CustomSwitch(
                    activeColor: ColorsHelper.PRIMARY_COLOR,
                    value: values,
                    onChanged: (value) {
                      print("VALUE : $value");
                      setState(() {
                        values = value;
                        if (values) {
                          print("VALUE : $value");
                          txtYesOrNo = UIStringHelper
                              .postTaskDetailsTaskareYouSureLabelYES;
                        } else {
                          txtYesOrNo = UIStringHelper
                              .postTaskDetailsTaskareYouSureLabelNO;
                        }
                        print("VALUE : $txtYesOrNo");
                      });
                    },
                  ),

                  // FSwitch(
                  //   width: 65.0,
                  //   height: 35.538,
                  //   color: ColorsHelper.COLOR_GREY_400,
                  //   openColor: ColorsHelper.PRIMARY_COLOR,
                  //   onChanged: (v) {
                  //     print("VALUE : $v");

                  //     values = v;
                  //     print("VALUE>> : $values");
                  //     if (values) {
                  //       txtYesOrNo =
                  //           UIStringHelper.postTaskDetailsTaskareYouSureLabelYES;
                  //     } else {
                  //       txtYesOrNo =
                  //           UIStringHelper.postTaskDetailsTaskareYouSureLabelNO;
                  //     }
                  //     print("VALUE>> : $txtYesOrNo");
                  //   },
                  //   closeChild: Text(
                  //     "NO",
                  //     style: TextStyle(color: ColorsHelper.WHITE, fontSize: 11),
                  //   ),
                  //   openChild: Text(
                  //     "YES",
                  //     style: TextStyle(color: ColorsHelper.WHITE, fontSize: 11),
                  //   ),
                  // ),
                ],
              ),
            ]))); 
}
}
