import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/model/taskpost/vehicle_type.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/section/section_richtext_view.dart';
import 'package:trux24/ui_widget/taskpost/list_vehicle_type.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/input_view.dart';
// import 'package:custom_switch/custom_switch.dart';
import 'package:trux24/widgets/switch/custom_switch.dart';
import 'package:trux24/widgets/switch/fswitch.dart';

class TaskPostDetailsScreen extends StatefulWidget {
  @override
  _TaskPostDetailsUI createState() => _TaskPostDetailsUI();
}

class _TaskPostDetailsUI extends State<TaskPostDetailsScreen> {
  List<VehicleType> mVehicleTypeList = new List();
  List<VehicleType> mVehicleCapacityList = new List();
  List<VehicleType> mVehicleSizeList = new List();
  bool values = false;
  String txtYesOrNo = UIStringHelper.postTaskDetailsTaskareYouSureLabelNO;
  TextEditingController _goodsTypeController = TextEditingController();
  TextEditingController _describeController = TextEditingController();

  final FocusNode _goodsTypePhone = FocusNode();
  final FocusNode _nodeDescribe = FocusNode();
  int _selectedCat = 0;

  @override
  void initState() {
    super.initState();
    mVehicleTypeList.add(new VehicleType(0, "All", ""));
    mVehicleTypeList.add(new VehicleType(1, "Open Truck", ""));
    mVehicleTypeList.add(new VehicleType(2, "Covered Van", ""));

    mVehicleCapacityList.add(new VehicleType(0, "All", ""));
    mVehicleCapacityList.add(new VehicleType(1, "1 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(2, "2 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(4, "3.5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(5, "5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(6, "7.5 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(7, "10 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(8, "15 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(9, "25 Ton", ""));
    mVehicleCapacityList.add(new VehicleType(10, "25+ Ton", ""));

    mVehicleSizeList.add(new VehicleType(0, "7 Feet", ""));
    mVehicleSizeList.add(new VehicleType(1, "10 Feet", ""));
    mVehicleSizeList.add(new VehicleType(2, "12 Feet", ""));
    mVehicleSizeList.add(new VehicleType(4, "16 Feet", ""));
    mVehicleSizeList.add(new VehicleType(5, "18 Feet", ""));
    mVehicleSizeList.add(new VehicleType(6, "22 Feet", ""));
    mVehicleSizeList.add(new VehicleType(7, "22+ Feet", ""));
    print(mVehicleTypeList);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 7, right: 7, top: 5, bottom: 5),
      child: Column(
          // center the children
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Gaps.vGap8,
            buildFSwitchWithChild(),
            Gaps.vGap5,
            SectionRichTextView(
              UIStringHelper.postTaskDetailsTruckTypeLabel,
              onPressed: () {},
              //  onPressed: () => pushNewPage(context, MovieHotPage()),
              more: " *",
              textColor: ColorsHelper.TRUX_GREY_4_COLOR,
              padding: EdgeInsets.all(1),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 55,
              margin: const EdgeInsets.only(top: 0),
              padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
              color: ColorsHelper.WHITE,
              child: ItemVehicleSelector(
                mVehicleTypeList: mVehicleTypeList,
              ),
            ),
            Gaps.vGap2,
            SectionRichTextView(
              UIStringHelper.postTaskDetailsTruckCapacityLabel,
              onPressed: () {},
              more: " *",
              textColor: ColorsHelper.TRUX_GREY_4_COLOR,
              padding: EdgeInsets.all(1),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 55,
              margin: const EdgeInsets.only(top: 0),
              padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
              color: ColorsHelper.WHITE,
              child: ItemVehicleSelector(
                mVehicleTypeList: mVehicleCapacityList,
              ),
            ),
            Gaps.vGap2,
            SectionRichTextView(
              UIStringHelper.postTaskDetailsTruckSizeLabel,
              onPressed: () {},
              more: " *",
              textColor: ColorsHelper.TRUX_GREY_4_COLOR,
              padding: EdgeInsets.all(1),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 55,
              margin: const EdgeInsets.only(top: 0),
              padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
              color: ColorsHelper.WHITE,
              child: ItemVehicleSelector(
                mVehicleTypeList: mVehicleSizeList,
              ),
            ),
            Gaps.vGap2,
            SectionRichTextView(
              UIStringHelper.postTaskDetailsSelectGoodsTypeLabel,
              onPressed: () {},
              more: "",
              textColor: ColorsHelper.TRUX_GREY_4_COLOR,
              padding: EdgeInsets.all(1),
            ),
            Container(
              //padding: EdgeInsets.all(3),
              height: 45,
              child: CustomTextField(
                focusNode: _goodsTypePhone,
                controller: _goodsTypeController,
                etEnabled: false,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(left: 5, top: 3),
                  hintText: UIStringHelper.postTaskDetailsSelectYourGoodsHint,
                  counterText: "",
                  filled: true,
                  fillColor: ColorsHelper.WHITE,
                  suffixIcon: _circularShape,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red, //this has no effect
                    ),
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide: BorderSide(
                        color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    borderSide: BorderSide(
                        color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                  ),
                ),
              ),
            ),
            Gaps.vGap2,
            SectionRichTextView(
              UIStringHelper.postTaskDetailsDescripbeTripLabel,
              onPressed: () {},
              more: " *",
              textColor: ColorsHelper.TRUX_GREY_4_COLOR,
              padding: EdgeInsets.all(0),
            ),
            CustomTextField(
              focusNode: _nodeDescribe,
              controller: _describeController,
              minLines: 5,
              maxLines: 15,
              etEnabled: true,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: UIStringHelper.postTaskDetailsDescriptionHint,
                counterText: "",
                filled: true,
                fillColor: ColorsHelper.WHITE,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red, //this has no effect
                  ),
                  borderRadius: BorderRadius.circular(0.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  borderSide:
                      BorderSide(color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR),
                ),
              ),
            ),
            Gaps.vGap10,
          ]),
    );
  }

  Widget _builderDesc() {
    return SliverList(
        delegate: SliverChildListDelegate(<Widget>[
      Gaps.vGap10,
      buildFSwitchWithChild(),
      Gaps.vGap10,
      RichText(
        // overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        text: TextSpan(
          children: [
            TextSpan(
                text: UIStringHelper.postTaskDetailsTruckTypeLabel,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                    fontSize: 13)),
            TextSpan(
                text: " *",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.COLOR_RED_DOT_DARK,
                    fontSize: 13)),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(top: 5, bottom: 7.5),
        padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
        height: 55,
        color: ColorsHelper.WHITE,
        child: ItemVehicleSelector(
          mVehicleTypeList: mVehicleTypeList,
        ),
      ),
      Gaps.vGap5,
      RichText(
        // overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        text: TextSpan(
          children: [
            TextSpan(
                text: UIStringHelper.postTaskDetailsTruckTypeLabel,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                    fontSize: 13)),
            TextSpan(
                text: " *",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.COLOR_RED_DOT_DARK,
                    fontSize: 13)),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(top: 5, bottom: 7.5),
        padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
        height: 55,
        color: ColorsHelper.WHITE,
        child: ItemVehicleSelector(
          mVehicleTypeList: mVehicleCapacityList,
        ),
      ),
      Gaps.vGap5,
      RichText(
        // overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        text: TextSpan(
          children: [
            TextSpan(
                text: UIStringHelper.postTaskDetailsTruckSizeLabel,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                    fontSize: 13)),
            TextSpan(
                text: " *",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.COLOR_RED_DOT_DARK,
                    fontSize: 13)),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(top: 5, bottom: 7.5),
        padding: EdgeInsets.fromLTRB(7, 8, 7, 8),
        height: 55,
        color: ColorsHelper.WHITE,
        child: ItemVehicleSelector(
          mVehicleTypeList: mVehicleSizeList,
        ),
      ),
      Gaps.vGap5,
      RichText(
        // overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        text: TextSpan(
          children: [
            TextSpan(
                text: UIStringHelper.postTaskDetailsSelectGoodsTypeLabel,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: ColorsHelper.TRUX_GREY_4_COLOR,
                    fontSize: 13)),
          ],
        ),
      ),
    ]));
  }

 
  Widget _builderContent() {
    return SliverToBoxAdapter(
        child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[])));
  }

  Widget _circularShape = new Container(
    margin: const EdgeInsets.all(7),
    height: 20,
    width: 20,
    padding: const EdgeInsets.all(3),
    decoration: BoxDecoration(
        border: Border.all(
            color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
            width: 1,
            style: BorderStyle.solid),
        shape: BoxShape.circle,
        color: Colors.white),
    child: Icon(
      Icons.chevron_right,
      color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
    ),
  );

  Container buildFSwitchWithChild() {
    return Container(
        // margin: const EdgeInsets.only(top: 5, bottom: 7.5),
        padding: EdgeInsets.all(4),
        color: ColorsHelper.WHITE,
        child: Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text('$txtYesOrNo',
                          style: TextStyle(
                              fontSize: 12,
                              color: ColorsHelper.AIR_RED_COLOR))),
                  CustomSwitch(
                    activeColor: ColorsHelper.PRIMARY_COLOR,
                    value: values,
                    onChanged: (value) {
                      print("VALUE : $value");
                      setState(() {
                        values = value;
                        if (values) {
                          print("VALUE : $value");
                          txtYesOrNo = UIStringHelper
                              .postTaskDetailsTaskareYouSureLabelYES;
                        } else {
                          txtYesOrNo = UIStringHelper
                              .postTaskDetailsTaskareYouSureLabelNO;
                        }
                        print("VALUE : $txtYesOrNo");
                      });
                    },
                  ),

                  // FSwitch(
                  //   width: 65.0,
                  //   height: 35.538,
                  //   color: ColorsHelper.COLOR_GREY_400,
                  //   openColor: ColorsHelper.PRIMARY_COLOR,
                  //   onChanged: (v) {
                  //     print("VALUE : $v");

                  //     values = v;
                  //     print("VALUE>> : $values");
                  //     if (values) {
                  //       txtYesOrNo =
                  //           UIStringHelper.postTaskDetailsTaskareYouSureLabelYES;
                  //     } else {
                  //       txtYesOrNo =
                  //           UIStringHelper.postTaskDetailsTaskareYouSureLabelNO;
                  //     }
                  //     print("VALUE>> : $txtYesOrNo");
                  //   },
                  //   closeChild: Text(
                  //     "NO",
                  //     style: TextStyle(color: ColorsHelper.WHITE, fontSize: 11),
                  //   ),
                  //   openChild: Text(
                  //     "YES",
                  //     style: TextStyle(color: ColorsHelper.WHITE, fontSize: 11),
                  //   ),
                  // ),
                ],
              ),
            ])));
  }
}
