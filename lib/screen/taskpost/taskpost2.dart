import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/screen/taskpost/taskpostbudget/taskpostbudget.dart';
import 'package:trux24/screen/taskpost/taskpostdetails/first.dart';
import 'package:trux24/screen/taskpost/taskpostdetails/second.dart';
import 'package:trux24/screen/taskpost/taskpostdetails/taskpostdetails.dart';
import 'package:trux24/screen/taskpost/taskpostduedate/taskpostduedate.dart';
import 'package:trux24/widgets/stepper/FAStep.dart';


class TaskPost2Screen extends StatefulWidget {
  @override
  _TaskPostStepperUI createState() => _TaskPostStepperUI();
}

class _TaskPostStepperUI extends State<TaskPost2Screen> {
  int currentStep = 0;
  int step;
  var SetState;
  VoidCallback mContinue;
  VoidCallback mCancel;
  ScrollController scrollController = new ScrollController();
  ScrollController _scrollController = new ScrollController();



  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
        backgroundColor: ColorsHelper.PRIMARY_COLOR_DARK,
      resizeToAvoidBottomPadding: false,
      
      appBar: AppBar(
        title: Text('Trip Post'),
       // centerTitle: true,
       backgroundColor: ColorsHelper.PRIMARY_COLOR,
      ),
     
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/bg_image/all_bg.png"),
              fit: BoxFit.cover,
            )),
      child :  Column(
        children: <Widget>[
          Expanded(
            child: FAStepper(
               // physics: ClampingScrollPhysics(),
        // Using a variable here for handling the currentStep
        currentStep: this.currentStep,
        // List the steps you would like to have
        titleHeight: 70,
        steps: _stepper(),
        // Define the type of Stepper style
        // StepperType.horizontal :  Horizontal Style
        // StepperType.vertical   :  Vertical Style
        type: FAStepperType.horizontal,
        titleIconArrange: FAStepperTitleIconArrange.row,
        stepNumberColor: ColorsHelper.PRIMARY_COLOR,
                physics: AlwaysScrollableScrollPhysics(),
                 
                onStepContinue: () {
                  setState(() {
                    if (currentStep < this._stepper().length - 1) {
                      currentStep = currentStep + 1;
                    } else {
                      currentStep = 0;
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (currentStep > 0) {
                      currentStep = currentStep - 1;
                    } else {
                      currentStep = 0;
                    }
                  });
                },
                onStepTapped: (step) {
                  setState(() {
                    currentStep = step;
                  });
                },
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  mContinue = onStepContinue;
                  mCancel = onStepCancel;
                  return Container();
                }),
          )
        ],
      ),
        // body: Column(
        //   children: <Widget>[
        //     Expanded(
        //       child: Stepper(
        //           type: StepperType.horizontal,
        //           physics: AlwaysScrollableScrollPhysics(),
        //           onStepContinue: () {
        //             setState(() {
        //               if (currentStep < this._stepper().length - 1) {
        //                 currentStep = currentStep + 1;
        //               } else {
        //                 currentStep = 0;
        //               }
        //             });
        //           },
        //           onStepCancel: () {
        //             setState(() {
        //               if (currentStep > 0) {
        //                 currentStep = currentStep - 1;
        //               } else {
        //                 currentStep = 0;
        //               }
        //             });
        //           },
        //           currentStep: this.currentStep,
        //           onStepTapped: (step) {
        //             setState(() {
        //               currentStep = step;
        //             });
        //           },
        //           steps: _stepper(),
        //           controlsBuilder: (BuildContext context,
        //               {VoidCallback onStepContinue,
        //               VoidCallback onStepCancel}) {
        //             mContinue = onStepContinue;
        //             mCancel = onStepCancel;
        //             return Container();
        //           }),
        //     )
        //   ],
        // ),
        ),
      bottomNavigationBar: new Container(
        // margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        color: ColorsHelper.PRIMARY_COLOR,
        child: new ConstrainedBox(
          constraints: const BoxConstraints.tightFor(height: 48.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new FlatButton(
                onPressed: () {
                  return mCancel();
                },
                textColor: Colors.white,
                textTheme: ButtonTextTheme.normal,
                child:  Visibility( 
                  visible: currentStep > 0 ?? false,
                  child: new Row(children: <Widget>[
                  const Icon(Icons.chevron_left),
                  const Text('BACK')
                ])),
              ),
              new FlatButton(
                onPressed: () {
                  return mContinue();
                },
                textColor: Colors.white,
                textTheme: ButtonTextTheme.normal,
                child: new Row(children: const <Widget>[
                  const Text('NEXT'),
                  const Icon(Icons.chevron_right)
                ]),
              )
            ],
          ),
        ),
      ),
      );
  }

  List<FAStep> _stepper() {
    List<FAStep> my_steps = [
       FAStep(
        // Title of the Step
          title: Padding(
            padding: EdgeInsets.only(right: 2.0),
            child: new Text('DETAILS'),
          ),
          content: TaskPostDetailsScreen(),
          // Padding(
          //   padding: EdgeInsets.all(0.0),
          //   child: TaskPostDetailsScreen(),
          // ),
          isActive: (currentStep >= 0),
          state: (currentStep > 0) ? FAStepstate.complete : FAStepstate.disabled),
       FAStep(
         title: Padding(
          padding: EdgeInsets.only(right: 2.0),
          child: new Text('DUE DATE'),
        ),
        content: TaskPostDueDateScreen(),
        isActive: (currentStep >= 0),
        state: (currentStep > 1) ? FAStepstate.complete : FAStepstate.disabled,),  
       FAStep(
         title: Padding(
          padding: EdgeInsets.only(right: 2.0),
          child: new Text('BUDGET'),
        ),
        content:  TaskPostBudgetScreen(),
        // Text("Hello World!"),
       
        isActive: (currentStep >= 0),
        state: (currentStep > 2) ? FAStepstate.complete : FAStepstate.disabled,),
    
    ];
        return my_steps;

  }

  // List<Step> _stepper() {
  //   List<Step> my_steps = [
  //     new Step(
  //         title: Padding(
  //           padding: EdgeInsets.only(right: 0.0),
  //           child: new Text('DETAILS'),
  //         ),
  //         content:Padding(
  //           padding: EdgeInsets.all(0.0),
  //           child: TaskPostDetailsScreen(),
  //         ),
  //         isActive: (currentStep >= 0),
  //         state: (currentStep == 0) ? StepState.complete : StepState.disabled),
  //     new Step(
  //       title: Padding(
  //         padding: EdgeInsets.only(right: 0.0),
  //         child: new Text('DUE DATE'),
  //       ),
  //       content: Text('this is the second step'),
  //       isActive: (currentStep >= 0),
  //       state: (currentStep == 1) ? StepState.complete : StepState.disabled,
  //     ),
  //     new Step(
  //       title: Padding(
  //         padding: EdgeInsets.only(right: 0.0),
  //         child: new Text('BUDGET'),
  //       ),
  //       content: new Text('this is third step'),
  //       isActive: (currentStep >= 0),
  //       state: (currentStep == 2) ? StepState.complete : StepState.disabled,
  //     ),
  //   ];
  //   return my_steps;
  // }
}
