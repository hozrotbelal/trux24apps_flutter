import 'package:flutter/material.dart';
import 'package:trux24/model/vehicle_category.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/ui_widget/menu_grid_view.dart';
import 'package:trux24/utils/utils.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeUI createState() => _HomeUI();
}

class _HomeUI extends State<HomeScreen> {
  List<String> corcess = new List();
  List<String> corcessImg = new List();
  List<VehicleCategory> vCategory = new List();

  @override
  void initState() {
    super.initState();
    vCategory.add(new VehicleCategory(
        "Open Pickup", "http://trux24.com/assets/img/1. mini_truck.png"));

    vCategory.add(new VehicleCategory("Medium Truck (Open)",
        "http://trux24.com/assets/img/3. medium_truck.png"));

    vCategory.add(new VehicleCategory("Heavy Truck (Open)",
        "http://trux24.com/assets/img/4. heavy_truck.png"));

    vCategory.add(new VehicleCategory("Covered Pickup",
        "http://trux24.com/assets/img/5. mini covered van.png"));

    vCategory.add(new VehicleCategory("Medium Covered van",
        "http://trux24.com/assets/img/7. Medium covered van.png"));

    vCategory.add(new VehicleCategory("Heavy covered van",
        "http://trux24.com/assets/img/8. Heavy covered van.png"));

    vCategory.add(new VehicleCategory(
        "Dump Truck", "http://trux24.com/assets/img/dump_truck.png"));

    vCategory.add(new VehicleCategory(
        "Prime mover", "http://trux24.com/assets/img/9. Prime Mover.png"));

    vCategory.add(new VehicleCategory("Special Vehicle",
        "https://trux24.com/assets/img/special_vehicle.png"));

    vCategory.add(new VehicleCategory(
        "Dump Truck", "http://trux24.com/assets/img/dump_truck.png"));

    vCategory.add(new VehicleCategory(
        "Prime mover", "http://trux24.com/assets/img/9. Prime Mover.png"));

    vCategory.add(new VehicleCategory("Special Vehicle",
        "https://trux24.com/assets/img/special_vehicle.png"));
    vCategory.add(new VehicleCategory(
        "Dump Truck", "http://trux24.com/assets/img/dump_truck.png"));

    vCategory.add(new VehicleCategory(
        "Prime mover", "http://trux24.com/assets/img/9. Prime Mover.png"));

    vCategory.add(new VehicleCategory("Special Vehicle",
        "https://trux24.com/assets/img/special_vehicle.png"));

    corcess.add("Mini Truck");
    corcess.add("Small Truck");
    corcess.add("Medium Truck");
    corcess.add("Heavy Truck");
    corcess.add("Mini covered");
    corcess.add("Small covered");
    corcess.add("Heavy covered Van");
    corcess.add("Prime move");
    corcess.add("Others");

    corcessImg.add("_1_mini_truck.png");
    corcessImg.add("_2_small_truck.png");
    corcessImg.add("_3_medium_truck.png");
    corcessImg.add("_4_heavy_truck.png");
    corcessImg.add("_5_mini_covered_van.png");
    corcessImg.add("_6_small_covered_van.png");
    corcessImg.add("_7_medium_covered_van.png");
    corcessImg.add("_8_heavy_covered_van.png");
    corcessImg.add("_9_heavy_covered_van.png");

    print(vCategory);
    print(vCategory.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsHelper.PRIMARY_COLOR,
          title: Text("Trux 24"),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                //  showSearch( context: context, delegate:GooglePlaySearch() );
              },
            ),
            // Icon(Icons.keyboard_voice),
            Padding(
              padding: EdgeInsets.only(right: 20.0),
            ),
          ],
        ),
        body: Container(
          // width: MediaQuery.of(context).size.width,
          // height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/bg_image/all_bg.png"),
            fit: BoxFit.cover,
          )),
          child: ListView(children: <Widget>[
            Padding(
              padding: EdgeInsets.all(0),
              child: Container(
                  width: Utils.width,
                  color: ColorsHelper.WHITE,
                  alignment: AlignmentDirectional.center,
                  child: Text('\n Version 1.0.0 \n')),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: Container(
                margin: EdgeInsets.only(bottom: 60),
                child: Wrap(
                  spacing: 5,
                  runSpacing: 5,
                  children: vCategory.length == 0
                      ? []
                      : vCategory.map((movie) => MenuGridView(movie)).toList(),
                ),
              ),
            )
          ]),
        )
    );
  }

  Widget prepareList(int k) {
    return Card(
      child: Hero(
        tag: "text$k",
        child: Material(
          child: InkWell(
            onTap: () {},
            child: Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Image.asset(
                      "assets/menu_image/_1_mini_truck.png",
                      width: 100,
                      height: 90,
                    ),
                    // Image.asset("assets/menu_image/"+corcessImg[k]),
                    Text(
                      corcess[k],
                      style: new TextStyle(
                          color: ColorsHelper.TRUX_GREY_4_COLOR, fontSize: 13),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),

                    Gaps.hGap3,
                    Text(
                      "(0-1 Ton)",
                      style: new TextStyle(
                          color: ColorsHelper.TRUX_GREY_4_COLOR, fontSize: 11),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCardMenuView() {
    return Card(
      child: Hero(
        tag: "text",
        child: Material(
          child: InkWell(
            onTap: () {},
            child: Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assets/menu_image/_1_mini_truck.png",
                        width: 100,
                        height: 90,
                      ),
                    ),
                    Text(
                      "Mini Truck",
                      style: new TextStyle(
                          color: ColorsHelper.TRUX_GREY_4_COLOR, fontSize: 13),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Gaps.hGap3,
                    Text(
                      "(0-1 Ton)",
                      style: new TextStyle(
                          color: ColorsHelper.TRUX_GREY_4_COLOR, fontSize: 11),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
