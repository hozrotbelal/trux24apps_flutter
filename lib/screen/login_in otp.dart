import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/res/styles.dart';
import 'package:trux24/screen/otp_screen.dart';
import 'package:trux24/utils/keyboard/keyboard_visibility.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:trux24/widgets/component/on_boarding_enter_animation.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
//  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginUI createState() => _LoginUI();
}

class _LoginUI extends State<LoginScreen> {
  TextEditingController _phoneController = TextEditingController();
  final FocusNode _nodePhone = FocusNode();

  KeyboardVisibilityNotification _keyboardVisibility =
      new KeyboardVisibilityNotification();
  int _keyboardVisibilitySubscriberId;
  bool _keyboardState;

  @protected
  void initState() {
    super.initState();

    _keyboardState = _keyboardVisibility.isKeyboardVisible;

    _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
      onChange: (bool visible) {
        setState(() {
          _keyboardState = visible;
        });
      },
    );
  }

  @override
  void dispose() {
        super.dispose();
    _keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;

     return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorsHelper.PRIMARY_COLOR,
//            leading: IconButton(
//                icon: Icon(
//                  Icons.list,
//                  color: Colors.white,
//                ),
//                onPressed: () {}),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/bg_image/all_bg.png"),
            fit: BoxFit.cover,
          )),

          /// Set component layout
          child: Column( 
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
            new Expanded(
              flex: 3,
              child: Stack(children: <Widget>[
               _buildHeaderCurveShape(),
                new Column(children: <Widget>[
                  new Expanded(
                    flex: 2,
                    child: _buildMobileImageView(),
                  
                  ),
                  new Expanded(
                    flex: 1,
                       child: _buildHeaderBottomTextView(),
                  ),
                    
                ])
              ]),

              // child: Stack(children: <Widget>[
              //   Column(mainAxisSize: MainAxisSize.max, children: <Widget>[

              //   ]),
              // ])
            ),
            new Expanded(
                flex: 4,
                child: Stack(children: <Widget>[
                  Column(children: <Widget>[
                    Gaps.vGap5,
                   new Expanded(
                                      flex: 7,
                                      child: Container(
                                        child: new Column(children: <Widget>[
                                          SizedBox(height: 5),
                                          _buildMobileInputView(),
                                          SizedBox(height: 2),
                                          _buildContinueButtonView(),
                                          Visibility(
                                              visible: !_keyboardState,
                                              child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    SizedBox(height: 5),
                                                    _buildOrHorizontalView(),
                                                    SizedBox(height: 5),
                                                    _buildSocialBtnRow()
                                                  ])),
                                        ]),
                                      )),
                                  Visibility(
                                      visible: !_keyboardState,
                                      child: Expanded(
                                          flex: 2,
                                          child: _buildBottomTextView())),
                  ])
                ]))
          ])),
    );
  }

  //   return Scaffold(
     
  //         appBar: AppBar(
  //           elevation: 0,
  //           backgroundColor: ColorsHelper.PRIMARY_COLOR,
  //            actions: <Widget>[
  //                   Text('Eng'),
  //                 ]
  //           //  leading: IconButton(
  //           //      icon: Icon(
  //           //        Icons.list,
  //           //        color: Colors.white,
  //           //      ),
  //           //      onPressed: () {}),
  //         ),
  //         body: new Container(
  //          decoration: BoxDecoration(
  //           image: DecorationImage(
  //               image: AssetImage("assets/bg_image/all_bg.png"),
  //               fit: BoxFit.cover)),
  //           child: new Column(
  //             crossAxisAlignment: CrossAxisAlignment.stretch,
  //             children: [
  //               new Expanded(
  //                   flex: 2,
  //                   child: Stack(children: <Widget>[
  //                     _buildHeaderCurveShape(),
  //                     new Column(children: <Widget>[
  //                       new Expanded(flex: 2, child: _buildMobileImageView()),
  //                       new Expanded(
  //                           flex: 1, child: _buildHeaderBottomTextView()),
  //                     ])
  //                   ])),
  //               new Expanded(
  //                   flex: 3,
  //                   child: Stack(
  //                     children: <Widget>[
  //                       new Container(
  //                           color: ColorsHelper.COLOR_BLUE_50,
  //                           child: Column(
  //                               crossAxisAlignment: CrossAxisAlignment.center,
  //                               mainAxisSize: MainAxisSize.max,
  //                               mainAxisAlignment: MainAxisAlignment.end,
  //                               children: <Widget>[
  //                                 new Expanded(
  //                                     flex: 7,
  //                                     child: Container(
  //                                       child: new Column(children: <Widget>[
  //                                         SizedBox(height: 5),
  //                                         _buildMobileInputView(),
  //                                         SizedBox(height: 2),
  //                                         _buildContinueButtonView(),
  //                                         Visibility(
  //                                             visible: !_keyboardState,
  //                                             child: Column(
  //                                                 mainAxisAlignment:
  //                                                     MainAxisAlignment.center,
  //                                                 children: <Widget>[
  //                                                   SizedBox(height: 5),
  //                                                   _buildOrHorizontalView(),
  //                                                   SizedBox(height: 5),
  //                                                   _buildSocialBtnRow()
  //                                                 ])),
  //                                       ]),
  //                                     )),
  //                                 Visibility(
  //                                     visible: !_keyboardState,
  //                                     child: Expanded(
  //                                         flex: 2,
  //                                         child: _buildBottomTextView()))
  //                               ]))
  //                     ],
  //                   )),
  //             ],
  //           ),
  //         ),
  //         //   bottomNavigationBar: Container(
  //         //       alignment: Alignment.center, height: 50, child: Text('Version')),
        
  //   );
  // }

  // Header CurveShape Widget
  Widget _buildHeaderCurveShape() {
    final String headerCurve = "assets/bg_image/head_otp_screen.svg";

    return Container(
        width: MediaQuery.of(context).size.width,
        color: ColorsHelper.COLOR_BLUE_50,
        child: SvgPicture.asset(
          headerCurve,
          color: ColorsHelper.PRIMARY_COLOR,
          fit: BoxFit.fill,
        ));
  }

  // Header Mobile Image Widget
  Widget _buildMobileImageView() {
    final String mobileImg = "assets/bg_image/mobile_icon.png";
    return Container(
        alignment: Alignment.bottomCenter,
        child: Container(
            width: 120,
            height: 120,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  mobileImg,
                ),
              ),
            )));
  }

  // Header Bottom Text Widget
  Widget _buildHeaderBottomTextView() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(bottom: 20),
      child: Text(
        UIStringHelper.pleaseGiveMobileNumber,
        style: TextStyle(fontSize: 20, color: ColorsHelper.WHITE),
      ), //variable above
    );
  }

  // Continue Button Widget
  Widget _buildContinueButtonView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: ButtonWidget(
          color: ColorsHelper.PRIMARY_COLOR,
          shadow: UIHelper.MUZ_BUTTONSHADOW,
          text: UIStringHelper.btnContinue,
          topPadding: 7,
          circleAvatar: true,
          onPressed: () {
            // Navigator.pop(context);
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => OtpScreen()));

            print("Tapped Hello");
          },
        ),
      ),
    );
  }

  // Monile input Widget
  Widget _buildMobileInputView() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50.0,
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            border: Border.all(
                width: 1,
                color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                style: BorderStyle.solid)),
        child: Row(children: <Widget>[
          Container(
              padding: new EdgeInsets.fromLTRB(10.0, 0.0, 8.0, 0.0),
              child: Image.asset('assets/country_image/bd.png',
                  width: 30, height: 20, fit: BoxFit.fill)),
          Container(
            padding: new EdgeInsets.only(right: 8.0),
            child: Text("+880",
                style: TextStyle(
                    fontSize: 16,
                    color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR)),
          ),
          Container(
              padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
              width: 1.0,
              height: 50.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                  ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR
                ]),
              )),
          Expanded(
              child: Container(
            height: 50.0,
            padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            child: CustomTextField(
                focusNode: _nodePhone,
                controller: _phoneController,
                maxLength: 11,
                keyboardType: TextInputType.phone,
                hintText: "017********"),
          )),
        ]));
  }

  /// Or Horizontal line
  Widget _buildOrHorizontalView() {
    return Padding(
        padding: const EdgeInsets.only(top: 5.0),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Container(
              width: 100.0,
              height: 1.0,
              decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.white10, Colors.white]),
              )),
          Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15.0),
              child: Text('OR',
                  style: TextStyle(
                      fontSize: 16,
                      color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR))),
          Container(
              width: 100.0,
              height: 1.0,
              decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.white, Colors.white10]),
              ))
        ]));
  }

  // Social Button Widget
  Widget _buildSocialBtnRow() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: CircleButton(
                fillColor: Color(0xff4C76BE),
                iconColor: ColorsHelper.WHITE,
                onPressedAction: () {
                  debugPrint('Login with Facebook');
                },
                icon: FontAwesome.facebook,
                elevation: 5.0,
                highlightElevation: 3,
                size: 45.0,
              )),
          Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: CircleButton(
                fillColor: Color(0xffFC5345),
                iconColor: ColorsHelper.WHITE,
                onPressedAction: () {
                  debugPrint('Login with Google');
                },
                icon: FontAwesome.google,
                elevation: 5.0,
                highlightElevation: 3,
                size: 45.0,
              )),
        ],
      ),
    );
  }

  // Terms and Condition Text Widget
  Widget _buildBottomTextView() {
    return Center(
        child: Padding(
            padding: EdgeInsets.all(3.0),
            child: Text.rich(
              TextSpan(
                text: UIStringHelper.loginParagraphTermsAndConditions,
                style: TextStyle(fontSize: 13),
                children: <TextSpan>[
                  TextSpan(
                      text: UIStringHelper
                          .loginParagraphTermsAndConditionsUnderline,
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          launch(UIStringHelper.trux24TermsConsitionlink);
                        },
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: ColorsHelper.COLOR_BLUE_900)),

                  // can add more TextSpans here...
                ],
              ),
              textAlign: TextAlign.center,
            )));
  }
}
