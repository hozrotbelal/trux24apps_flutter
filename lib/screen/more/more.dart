import 'package:flutter/material.dart';
import 'package:trux24/model/option_list_item.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/ui_widget/more/more_item_view.dart';
import 'package:trux24/utils/utils.dart';
import 'package:trux24/utils/values/strings.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreSUI createState() => _MoreSUI();
}

class _MoreSUI extends State<MoreScreen> {
 
  List<OptionListItem> vOptionItem = new List();

  @override
  void initState() {
    super.initState();
    vOptionItem.add(new OptionListItem(UIStringHelper.dashboardScreenTitle, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionProfileLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionMyBalanceLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionPromotionsLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionPaymentHistoryLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionPaymentMethodsLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionReviewsabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionNotificationsLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionTaskAlertsLabel,""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionSettingsLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionHelpLabel, ""));

    vOptionItem.add(new OptionListItem(UIStringHelper.moreOptionLogoutLabel, ""));


   
    print(vOptionItem);
    print(vOptionItem.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsHelper.PRIMARY_COLOR,
          title: Text("Trux 24"),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                //  showSearch( context: context, delegate:GooglePlaySearch() );
              },
            ),
            // Icon(Icons.keyboard_voice),
            Padding(
              padding: EdgeInsets.only(right: 20.0),
            ),
          ],
        ),
        body: Container(
          // width: MediaQuery.of(context).size.width,
          // height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/bg_image/all_bg.png"),
            fit: BoxFit.cover,
          )),
          child: ListView(children: <Widget>[
        
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                child: Wrap(
                
                  children: vOptionItem.length == 0
                      ? []
                      : vOptionItem.map((vOptionListItem) => MoreItemView(vOptionListItem)).toList(),
                ),
              ),
            )
          ]),
        ));
  }

 
}
