import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/screen/home/home.dart';
import 'package:trux24/screen/more/more.dart';
import 'package:trux24/screen/profile/profile_screen2.dart';
import 'package:trux24/screen/task_tab/tasktab.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:trux24/screen/taskdetails/taskdetails.dart';
import 'package:trux24/screen/taskpost/taskpostdetails/taskpostdetails.dart';

class MainMenuScreen extends StatefulWidget {
  @override
  _MainMenuUI createState() => _MainMenuUI();
}

class _MainMenuUI extends State<MainMenuScreen> {
int _currentIndex = 0;
  List<Widget> list = List();  
  @protected
  void initState() {
    super.initState();

    list 
      ..add(HomeScreen())
      ..add(MyTaskTab())
      ..add(TaskDetailsScreen())
      ..add(Profile2Screen())
      ..add(MoreScreen());
      super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: list[_currentIndex],

        bottomNavigationBar: ConvexAppBar.badge(
          {3: '99'},
          badgePadding: EdgeInsets.all(2),
          badgeColor: ColorsHelper.COLOR_RED_400,
          badgeBorderRadius: 30,
          items: [
            TabItem(icon: Icons.home, title: 'Trip Post'),
            TabItem(icon: Icons.map, title: 'My Trip'),
            TabItem(icon: Icons.search, title: 'Browse'),
            TabItem(icon: Icons.message, title: 'Message'),
            TabItem(icon: Icons.more, title: 'More'),
          ],

          backgroundColor: ColorsHelper.PRIMARY_COLOR,
          initialActiveIndex: _currentIndex, //optional, default as 0

        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        }

         //           onTap: (int i) => print('click index=$i'),

        )
        // bottomNavigationBar: Container(
        //     height: 60,
        //     child: CupertinoTabBar(
        //         items: <BottomNavigationBarItem>[
        //           BottomNavigationBarItem(

        //               icon:Icon(Icons.dashboard,size: 20,),

        //               title: Text('Trip Post', style: TextStyle(fontSize: 12))),
        //           BottomNavigationBarItem(
        //                icon: Icon(Icons.trip_origin,size: 20),

        //               title: Text('My Trip', style: TextStyle(fontSize: 12))),
        //           BottomNavigationBarItem(
        //               icon: Icon(Icons.search,size: 20),

        //               title: Text('Browse', style: TextStyle(fontSize: 12))),

        //                BottomNavigationBarItem(
        //               icon: Icon(Icons.message,size: 20),

        //               title: Text('Message', style: TextStyle(fontSize: 12))),
        //           BottomNavigationBarItem(
        //             title: new Text('More', style: TextStyle(fontSize: 12)),
        //             icon: new Stack(children: <Widget>[
        //               new Icon(Icons.more,size: 20),
        //               new Positioned(
        //                 right: 0,
        //                 child: new Container(
        //                   padding: EdgeInsets.all(1),
        //                   decoration: new BoxDecoration(
        //                     color: Colors.red,
        //                     borderRadius: BorderRadius.circular(6),
        //                   ),
        //                   constraints: BoxConstraints(
        //                     minWidth: 12,
        //                     minHeight: 12,
        //                   ),
        //                   child: new Text(
        //                     '10',
        //                     style: new TextStyle(
        //                       color: Colors.white,
        //                       fontSize: 12,
        //                     ),
        //                     textAlign: TextAlign.center,
        //                   ),
        //                 ),
        //               )
        //             ]),
        //           ),
        //         ],
        //         backgroundColor: Colors.white,
        //         inactiveColor: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR,
        //         activeColor: ColorsHelper.PRIMARY_COLOR,
        //         currentIndex: _tabIndex,

        //         border: Border(
        //           top: BorderSide(
        //             color: ColorsHelper.TRUX_GREY_3_COLOR,
        //             width: 1.5, // One physical pixel.
        //             style: BorderStyle.solid,

        //           ),
        //         ),
        //         onTap: (index) => setState(() => _tabIndex = index)))

        );
  }
}
