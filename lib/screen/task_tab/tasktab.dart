import 'package:flutter/material.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/screen/mytask/mytask.dart';


class MyTaskTab extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Google Play Clone',
      theme: ThemeData(
        primaryColor: Color(0xff01865f),
      ),
      home: TaskTabScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class TaskTabScreen extends StatefulWidget {
    TaskTabScreen({Key key}) : super(key: key);

  @override
  _TaskTabUI createState() => _TaskTabUI();
}

class _TaskTabUI extends State<TaskTabScreen> with SingleTickerProviderStateMixin{
  TabController _controller;

  List<Tab> tabs = [
    Tab(text: 'All'),
    Tab(text: 'Posted'),
    Tab(text: 'Draft'),
    Tab(text: 'Assigned'),
    Tab(text: 'Offered'),
    Tab(text: 'Completed')
  ];

  
  @override
  void initState() {
    super.initState();
  _controller = TabController(length: tabs.length, vsync: this);
  //      _controller = new TabController( vsync: this, initialIndex: 0, length: 6);
  }

    @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {


    return Scaffold(
         appBar: AppBar(
                   backgroundColor: ColorsHelper.PRIMARY_COLOR,

        title:Text("Trux 24"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
            //  showSearch( context: context, delegate:GooglePlaySearch() );
            },
          ),
         // Icon(Icons.keyboard_voice),
          Padding( padding: EdgeInsets.only(right: 20.0), ),
        ],
        bottom: new TabBar(
          controller: _controller,
          indicatorColor: Colors.white,
          isScrollable: true,
          tabs: tabs,
          // tabs:<Widget>[
      
          //   new Tab(text: "All"),
          //   new Tab(text: "Posted"),
          //   new Tab(text: "Draft"),
          //   new Tab(text: "Assigned"),
          //   new Tab(text: "Offered"),
          //   new Tab(text: "Completed"),
          // ],
        ),
      ),

       body: TabBarView(
        controller: _controller,
        children: <Widget>[
                new MyTaskScreen(),
                new MyTaskScreen(),
                new MyTaskScreen(),
                new MyTaskScreen(),
                new MyTaskScreen(),
                new MyTaskScreen(),
        ],
      ),   
      // body: Container(
      //     width: MediaQuery.of(context).size.width,
      //     height: MediaQuery.of(context).size.height,
      //     decoration: BoxDecoration(
      //         image: DecorationImage(
      //       image: AssetImage("assets/bg_image/all_bg.png"),
      //       fit: BoxFit.cover,
      //     )),

      //      child : Column(
      //     children: <Widget>[
      //      // buildSearchBar(context),
      //       Expanded(
      //         child: TabBarView(controller: _controller, children: [
      //           new MyTaskScreen(),
      //           new MyTaskScreen(),
      //           new MyTaskScreen(),
      //           new MyTaskScreen(),
      //           new MyTaskScreen(),
      //           new MyTaskScreen(),
      //         ]))
      //     ]),
              
      //  )
    );
  }
}