import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:trux24/utils/keyboard/keyboard_visibility.dart';
import 'package:trux24/res/color_constant.dart';
import 'package:trux24/screen/otp_screen.dart';
import 'package:trux24/utils/ui_helper.dart';
import 'package:trux24/utils/values/strings.dart';
import 'package:trux24/widgets/button_widgets.dart';
import 'package:trux24/widgets/circle_button.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:trux24/widgets/input_view.dart';
import 'package:url_launcher/url_launcher.dart';

class KeyboardVisibilityExample extends StatefulWidget {
  KeyboardVisibilityExample({Key key}) : super(key: key);

  @override
  _KeyboardVisibilityExampleState createState() =>
      _KeyboardVisibilityExampleState();
}

class _KeyboardVisibilityExampleState extends State<KeyboardVisibilityExample> {
  TextEditingController _phoneController = TextEditingController();
  final FocusNode _nodePhone = FocusNode();

  KeyboardVisibilityNotification _keyboardVisibility =
      new KeyboardVisibilityNotification();
  int _keyboardVisibilitySubscriberId;
  bool _keyboardState;

  @protected
  void initState() {
    super.initState();

    _keyboardState = _keyboardVisibility.isKeyboardVisible;

    _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
      onChange: (bool visible) {
        setState(() {
          _keyboardState = visible;
        });
      },
    );
  }

  @override
  void dispose() {
    _keyboardVisibility.removeListener(_keyboardVisibilitySubscriberId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Keyboard visibility example'),
      ),
      body: Center(
        child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Input box for keyboard test',
                  ),
                ),
                Container(height: 60.0),
                Visibility(
                    visible: !_keyboardState,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 5),
                          _buildOrHorizontalView(),
                          SizedBox(height: 5),
                          _buildSocialBtnRow()
                        ])

                    //   child:  Text(
                    //   'The current state of the keyboard is: ' ,
                    // )
                    ),
               
              ],
            )),
      ),
    );
  }

  // Continue Button Widget
  Widget _buildContinueButtonView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: ButtonWidget(
          color: ColorsHelper.PRIMARY_COLOR,
          shadow: UIHelper.MUZ_BUTTONSHADOW,
          text: UIStringHelper.btnContinue,
          topPadding: 7,
          circleAvatar: true,
          onPressed: () {
            // Navigator.pop(context);
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => OtpScreen()));

            print("Tapped Hello");
          },
        ),
      ),
    );
  }

  // Monile input Widget
  Widget _buildMobileInputView() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50.0,
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            border: Border.all(
                width: 1,
                color: ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                style: BorderStyle.solid)),
        child: Row(children: <Widget>[
          Container(
              padding: new EdgeInsets.fromLTRB(10.0, 0.0, 8.0, 0.0),
              child: Image.asset('assets/country_image/bd.png',
                  width: 30, height: 20, fit: BoxFit.fill)),
          Container(
            padding: new EdgeInsets.only(right: 8.0),
            child: Text("+880",
                style: TextStyle(
                    fontSize: 16,
                    color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR)),
          ),
          Container(
              padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
              width: 1.0,
              height: 50.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR,
                  ColorsHelper.TRUX_GREY_SHOHOKAEY_COLOR
                ]),
              )),
          Expanded(
              child: Container(
            height: 50.0,
            padding: new EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            child: CustomTextField(
                focusNode: _nodePhone,
                controller: _phoneController,
                maxLength: 11,
                keyboardType: TextInputType.phone,
                hintText: "017********"),
          )),
        ]));
  }

  /// Or Horizontal line
  Widget _buildOrHorizontalView() {
    return Padding(
        padding: const EdgeInsets.only(top: 5.0),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Container(
              width: 100.0,
              height: 1.0,
              decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.white10, Colors.white]),
              )),
          Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15.0),
              child: Text('OR',
                  style: TextStyle(
                      fontSize: 16,
                      color: ColorsHelper.TRUX_GREY_DARK_LIGHT_COLOR))),
          Container(
              width: 100.0,
              height: 1.0,
              decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.white, Colors.white10]),
              ))
        ]));
  }

  // Social Button Widget
  Widget _buildSocialBtnRow() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: CircleButton(
                fillColor: Color(0xff4C76BE),
                iconColor: ColorsHelper.WHITE,
                onPressedAction: () {
                  debugPrint('Login with Facebook');
                },
                 widget:  Icon(
                    FontAwesome.google,
                    color: ColorsHelper.WHITE ?? Theme.of(context).accentColor,
                    size: 20,
                  ),
                elevation: 5.0,
                highlightElevation: 3,
                size: 45.0,
              )),
          Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: CircleButton(
                fillColor: Color(0xffFC5345),
                iconColor: ColorsHelper.WHITE,
                onPressedAction: () {
                  debugPrint('Login with Google');
                },
                 widget:  Icon(
                    FontAwesome.google,
                    color: ColorsHelper.WHITE ?? Theme.of(context).accentColor,
                    size: 20,
                  ),
                elevation: 5.0,
                highlightElevation: 3,
                size: 45.0,
              )),
        ],
      ),
    );
  }

  // Terms and Condition Text Widget
  Widget _buildBottomTextView() {
    return Center(
        child: Padding(
            padding: EdgeInsets.all(3.0),
            child: Text.rich(
              TextSpan(
                text: UIStringHelper.loginParagraphTermsAndConditions,
                style: TextStyle(fontSize: 13),
                children: <TextSpan>[
                  TextSpan(
                      text: UIStringHelper
                          .loginParagraphTermsAndConditionsUnderline,
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          launch(UIStringHelper.trux24TermsConsitionlink);
                        },
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: ColorsHelper.COLOR_BLUE_900)),

                  // can add more TextSpans here...
                ],
              ),
              textAlign: TextAlign.center,
            )));
  }
}
